#!/bin/bash

rm -f find_running_jobs_in_jobarray.txt
for i in $(seq 0 23)
do

	JOB_NAME="cuttlefisha_sra-aarch64-Mar19-2024-120324-run62-vague01"
	JOB_QUEUE="IndexThePlanetJobQueueGraviton$i" 

	id=$(aws batch list-jobs --job-status PENDING --job-queue "$JOB_QUEUE" | jq --arg JOB_NAME "$JOB_NAME" -r '.jobSummaryList[] | select(.jobName == $JOB_NAME) | .jobId')

# check there is just one id
# Convert the job IDs into an array
ids=($id)

# Check the number of job IDs
if [ "${#ids[@]}" -eq 1 ]; then
	echo "Found exactly one job ID: ${ids[0]}"
elif [ "${#ids[@]}" -eq 0 ]; then
	echo "No job IDs found for job name: $JOB_NAME"
	continue
else
	echo "Error: Found more than one job ID in job queue: $JOB_QUEUE"
	continue
fi

jobIds=$(aws batch list-jobs --array-job-id "$id" | jq -r '.jobSummaryList[] | select(.status == "RUNNING") | .jobId')

# Loop through each job ID to describe the job and get environment variables
for jobId in $jobIds; do
	echo "Fetching environment variables for Job ID: $jobId"
	# Use AWS CLI and jq to extract the S3_PATH environment variable for the job
	S3_PATH=$(aws batch describe-jobs --jobs ${jobId} | \
		jq -r --arg JOB_ID "$JOB_ID" '.jobs[].container.environment | .[] | select(.name == "S3_PATH") | .value')

	# Extract the index number after the colon in the job ID
	INDEX=$((${jobId##*:}+1))

	# Download the file from S3
	aws s3 cp "$S3_PATH" downloaded_file.txt

	# Extract the specific line corresponding to the index
	# Assuming the line number corresponds directly to the index
	LINE=$(sed "${INDEX}q;d" downloaded_file.txt)

	# Assign the line to a variable
	VAR="$LINE"

	# Display the variable content
	echo "$VAR"
	echo $VAR >> find_running_jobs_in_jobarray.txt

	rm -f downloaded_file.txt
done

done

