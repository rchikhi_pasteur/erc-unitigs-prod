import subprocess

def read_file_in_batches(file_path, batch_size):
    with open(file_path, 'r') as file:
        batch = []
        for line in file:
            line = line.strip()
            if line:  # Make sure the line is not empty
                batch.append(line)
            if len(batch) == batch_size:
                yield batch
                batch = []
        if batch:  # Yield the last batch if it's not empty
            yield batch

def terminate_instances_in_batches(file_path, batch_size=1000):
    for batch in read_file_in_batches(file_path, batch_size):
        instance_ids = ' '.join(batch)
        command = f"aws ec2 terminate-instances --instance-ids {instance_ids}"
        print(command)  # This prints the command. To execute, uncomment the next line.
        subprocess.run(command, shell=True)

# Example usage
file_path = 'instances_to_kill.txt'  # Path to your file
terminate_instances_in_batches(file_path)

