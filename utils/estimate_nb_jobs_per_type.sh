set=191223-500k

rm -f array_8gb.txt  array_15gb.txt  array_31gb.txt  array_63gb.txt 
# Execute helper grouping script and save outputs to separate files based on accession_size
python ../group_accessions.py <(zstdcat ../Athena_Dec_10_public.csv.zst  | awk -F, '{gsub(/"/, "", $1); gsub(/"/, "", $2); print $1"\t"$2}' |grep -v mbases ) |while IFS=$'\t' read -r accessions size; do
#python ../group_accessions.py ../sets/$set.tsv  |while IFS=$'\t' read -r accessions size; do
        if (( size < 20000 )); then
                echo "$accessions $size" >> array_8gb.txt
        elif (( size >= 20000 )) && (( size < 60000 )); then
                echo "$accessions $size" >> array_15gb.txt
        elif (( size >= 60000 )) && (( size < 200000 )); then
                echo "$accessions $size" >> array_31gb.txt
        else
                echo "$accessions $size" >> array_63gb.txt
        fi
done
