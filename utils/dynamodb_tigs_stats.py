#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# this one parses the Logan table and get aggregates
# let's see how quick this is, and how painful in terms of costs
# well, it's not fast. 2 minutes for 2.7M entries


import sys
import boto3

# Initialize a DynamoDB client
dynamodb = boto3.resource('dynamodb')

table = dynamodb.Table('Logan')

from datetime import datetime
startTime = datetime.now()

scan_kwargs = {
       'ProjectionExpression': 'accession, ' + # ARM64_return_value, ARM64_processing_date, ARM64_processing_time, '+
                               'seqstats_contigs_n50, seqstats_contigs_maxlen, seqstats_unitigs_n50, ' +
                               'seqstats_unitigs_maxlen, seqstats_unitigs_sumlen, seqstats_contigs_sumlen, ' +
                               'seqstats_unitigs_nbseq, seqstats_contigs_nbseq'
       }

print("accession,seqstats_unitigs_nbseq,seqstats_unitigs_sumlen")

# Structure to hold accession counts and instance types
sum_input = 0
while True:
    response = table.scan(**scan_kwargs)
    items = response['Items']
    for item in items:
        accession = item['accession']
        sum_input += 1
        if 'seqstats_unitigs_sumlen' not in item or 'seqstats_unitigs_nbseq' not in item: continue
        print(item['accession'],item['seqstats_unitigs_nbseq'],item['seqstats_unitigs_sumlen'],sep=',')

    #copy_to_another_table = True
    #if copy_to_another_table:
    #    for item in items:
    #        accession = item['accession']
    #        if 'seqstats_unitigs_sumlen' not in item or 'seqstats_unitigs_nbseq' not in item: continue
    #         print(item['accession'],item['seqstats_unitigs_nbseq'],item['seqstats_unitigs_sumlen'],sep=',')


    # Check if there are more items to fetch
    if 'LastEvaluatedKey' in response:
        scan_kwargs['ExclusiveStartKey'] = response['LastEvaluatedKey']
    else:
        break  # Exit the loop if no more items

sys.stderr.write(str(datetime.now() - startTime) + " seconds\n")

sys.stderr.write("nb items: " + str(sum_input) + "\n")

