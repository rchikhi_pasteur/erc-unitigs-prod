for nb in {0..23}; do
jobqueue="IndexThePlanetJobQueueGraviton$nb"

for jobId in $(aws batch list-jobs --job-queue $jobqueue --job-status RUNNING | jq -r '.jobSummaryList[] | .jobId'); do
    aws batch terminate-job --job-id $jobId --reason "Terminating job by manual request (kill_all_running_jobs.sh)"
    echo "killed $jobId"
done
done
