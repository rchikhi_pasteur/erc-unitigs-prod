import boto3
from botocore import exceptions
import queue
import threading
import time

# La file d'attente partagée
q = queue.Queue()

PAGE_SIZE = 1000
PREFIX_NAME_INSTANCES ="IndexThePlanet-Unitigs-SPOT"

ec2 = boto3.client('ec2')


def add_instance_id_to_queue(the_queue, response):
    if "Reservations" not in response:
        print("no reservations in response")
        return
    reservations = response["Reservations"]
    for reservation in reservations:
        instances = reservation["Instances"]
        for instance in instances:
            the_queue.put(instance["InstanceId"])

def build_describe_params(values, max_results, next_token):
    params = {
        "Filters": [
            {
                'Name': 'tag:Name',
                'Values': values
            },
            {
                'Name': 'instance-state-name',
                'Values': ['running']  # Modifier selon les états souhaités
            },
        ],
        "MaxResults": max_results
    }
    if next_token:
        params["NextToken"] = next_token
    return params

def describe_instances(values:list, max_results:int, next_token=None):
    params = build_describe_params(values, max_results, next_token)
    try:
        response = ec2.describe_instances(
            **params
        )
        return response
    except exceptions.ClientError as e:
        print("Exception with describe_instances", e)
        time.sleep(1)

def terminate_instances(instances_id: list):
    try:
        print(f"deleting {len(instances_id)} instances..")
        response = ec2.terminate_instances(InstanceIds=instances_id)
        print(f"deleted {len(instances_id)} instances")
    except exceptions.ClientError as e:
        print("Exception with terminate_instances", e)
        time.sleep(1)


def producteur():
    # Créer un client EC2

    # Lister toutes les instances
    response = describe_instances([f"{PREFIX_NAME_INSTANCES}*"], PAGE_SIZE, next_token=None)
    add_instance_id_to_queue(q, response)

    while "NextToken" in response:
        response = describe_instances([f"{PREFIX_NAME_INSTANCES}*"], PAGE_SIZE, next_token=response['NextToken'])
        add_instance_id_to_queue(q, response)
        print("queried instances IDs")


def consommateur(chunk_size):
    while True:
        chunk = []
        while len(chunk) < chunk_size:
            try:
                item = q.get_nowait()  # Essayer de retirer sans bloquer
                if item is None:
                    q.task_done()
                    return  # Sortir si le marqueur de fin est rencontré
                chunk.append(item)
                q.task_done()
            except queue.Empty:
                break  # Sortir de la boucle si la queue est vide
        if chunk:
            terminate_instances(chunk)


# Création des threads
t1 = threading.Thread(target=producteur)
t2 = threading.Thread(target=consommateur, args=(PAGE_SIZE,))

# Démarrage des threads
t1.start()
t2.start()

# Attendre que le producteur termine
t1.join()

# Attendre que toutes les tâches soient terminées
q.join()

# Arrêter le consommateur
q.put(None)
t2.join()

print('Fin du programme.')
