d_jobid = dict()
d_accessions = dict()
d_accession_type = dict()

for line in open("cloudwatch_batch_logs_analysis.accessions_.acc.tsv"):
    acc, size = line.split()
    size = int(size)
    if size < 20000:
        d_accession_type[acc] = '4vcpus'
    elif size >= 20000 and size < 60000: 
        d_accession_type[acc] = '8vcpus'
    else:
        print("huh, big accession shouldn't have been submitted:",acc)

print("parsed tsv")

d_complete_times = dict()
overhead = 0
total_job_times = 0
total_job_times_4vcpus = 0
total_job_times_8vcpus = 0
s_4vcpus_accessions = set()
s_8vcpus_accessions = set()
n_unknown_accessions = 0

for line in open("cloudwatch_batch_logs_analysis.complete_time.txt"):
    ls = line.split()
    if ls[-2] == "k-mer":
        #that's cuttlefish
        continue
    elif (ls[-2] == "request" and ls[-1] == "rate.") or ls[-1] == "Unknown":
        #that's S3 rate limits
        continue
    elif not ls[-2].isdigit():
        print("unexpected line:",line)
        continue
    jobid, accession, time = ls[0], ls[1], int(ls[-2])
    if jobid not in d_jobid:
        d_jobid[jobid] = set([accession])
    else:
        if accession in d_jobid[jobid]:
            print(f"job id {jobid} assigned has accession {accession} assigned twice to it")
        else:
            d_jobid[jobid].add(accession)
    if accession not in d_accessions:
        d_accessions[accession] = [jobid]
    else:
        d_accessions[accession] += [jobid]
        times = len(d_accessions[accession])
        if times > 2:
            #print(f"accession {accession} processed {times} times")
            pass

    total_job_times += time
    if accession in d_complete_times:
        #print(f"accession {accession} processed another time, took {time} vs {d_complete_times[accession]}")
        overhead += min(time,d_complete_times[accession])
    d_complete_times[accession] = time
    if accession not in d_accession_type:
        n_unknown_accessions += 1
    else:
        if d_accession_type[accession] == '4vcpus':
            total_job_times_4vcpus += time
            s_4vcpus_accessions.add(accession)
        else:
            total_job_times_8vcpus += time
            s_8vcpus_accessions.add(accession)

print(n_unknown_accessions,"unknown accessions for size")
print("total job times",total_job_times,"seconds")
print(len(s_4vcpus_accessions),"4vcpus jobs",total_job_times_4vcpus,"seconds")
print(len(s_8vcpus_accessions),"8vcpus jobs",total_job_times_8vcpus,"seconds")
print("wasted overhead",overhead,"seconds")

nb_incomplete_jobs = 0
nb_incomplete_accessions= 0
example_stealth_jobs = []
example_stealth_accessions = []
for line in open("cloudwatch_batch_logs_analysis.accessions_.txt"):
    ls = line.split()
    jobid, accession = ls[0], ls[-1]
    if jobid not in d_jobid:
        #print("stealth job did not complete",jobid)
        nb_incomplete_jobs += 1
        if len(example_stealth_jobs) < 5:
            example_stealth_jobs += [jobid]
        if accession in d_accessions:
            nb_incomplete_accessions += 1
            if len(example_stealth_accessions) < 5:
                example_stealth_accessions += [(accession,jobid)]
print(f"{nb_incomplete_jobs} stealth jobs did not complete up to the 'Completed' message")
print(f"{nb_incomplete_accessions} stealth accessions duplicated and did not complete up to the 'Completed' message")
print("for example:",example_stealth_jobs)
print("for example:",example_stealth_accessions)

