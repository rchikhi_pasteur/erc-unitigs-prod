# grabs the "accession -" lines. Let's see if that's useful. The "Complete pipeline processing time query below also displays accession but may not be displayed if carsh
# it also grabs dynamodb rate limits..
awslogs get /aws/batch/job ALL -s1d -G -f "accession" > cloudwatch_batch_logs_analysis.accessions.txt
grep "accession -" cloudwatch_batch_logs_analysis.accessions.txt > cloudwatch_batch_logs_analysis.accessions_.txt

# grabs both accession and pipeline time, provided there was no crash
awslogs get /aws/batch/job ALL -s1d -G --timestamp -f "Complete" > cloudwatch_batch_logs_analysis.complete_time.txt

# then run ananlype.py
python cloudwatch_batch_logs_analysis.analyze.py
