import boto3

def aws_batch_client():
    """Initialize and return an AWS Batch client."""
    return boto3.client('batch')

def list_compute_environments(client):
    """List all compute environments."""
    response = client.describe_compute_environments()
    return [env['computeEnvironmentName'] for env in response['computeEnvironments']]

def disable_compute_environment(client, compute_env_name):
    """Disable a specified compute environment."""
    client.update_compute_environment(computeEnvironment=compute_env_name, state='ENABLED')

def main():
    client = aws_batch_client()
    compute_envs = list_compute_environments(client)
    print("Available Compute Environments:", compute_envs)

    for compute_env_name in compute_envs:
        try:
            disable_compute_environment(client, compute_env_name)
            print(f"Compute environment '{compute_env_name}' has been re-enabled.")
        except:
            print(f"problem with that one, skipping {compute_env_name}")

if __name__ == "__main__":
    main()
