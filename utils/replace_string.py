import os
def replace_string(input_file, old_str, new_str):
	temp_file = input_file + '.tmp'
	with open(input_file, 'r') as file_in, open(temp_file, 'w') as file_out:
		for line in file_in:
			replaced_line = line.replace(old_str, new_str)
			file_out.write(replaced_line)
	# Rename the temporary file to overwrite the original file
	os.rename(temp_file, input_file)

import sys
input_file = sys.argv[1]
old_str = sys.argv[2]
new_str = sys.argv[3]

replace_string(input_file, old_str, new_str)
