#!/bin/bash

# Directory containing .sra files
DIRECTORY="$HOME/scratch/logan_sra"

# Check if there are .sra files in the directory
if compgen -G "$DIRECTORY/*.sra" > /dev/null; then
    # Loop through each .sra file in the directory
    for sra_file in "$DIRECTORY"/*.sra; do
		accession=$(basename "$sra_file" .sra)
        # Submit a job for this file
        sbatch  -o $PWD/slurm-logs/slurm-%j.out -e $PWD/slurm-logs/slurm-%j.err -q seqbio -p seqbio --mem 150G -c 8 $PWD/same_pipeline_but_local.sh "$accession"
    done 
else
    echo "No .sra files found in $DIRECTORY"
fi

