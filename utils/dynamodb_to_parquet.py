# warning: this needs 192 GB ram for some reason

import boto3
import pandas as pd
import json
import gzip
import os

# Initialize S3 client
s3 = boto3.client('s3')

# Define your bucket name and the prefix for the data directory
bucket_name = 'logan-athenaqueries'
data_prefix = 'AWSDynamoDB/01717527404957-a42322bc/data/'

# List all the objects in the data directory
response = s3.list_objects_v2(Bucket=bucket_name, Prefix=data_prefix)


# Ensure the response contains 'Contents'
if 'Contents' in response:
    # Filter out any directories (keys ending with '/')
    files = [obj['Key'] for obj in response['Contents'] if not obj['Key'].endswith('/')]

    # Initialize an empty list to store data
    combined_data = []

    # Download and read each file
    for file_key in files:
        # Download the file
        s3.download_file(bucket_name, file_key, 'temp.json.gz')
        
        # Decompress the file and read line by line
        with gzip.open('temp.json.gz', 'rt') as gz_file:
            for line in gz_file:
                data = json.loads(line)
                combined_data.append(data)

    # Convert the combined data to a DataFrame
    df = pd.DataFrame(combined_data)

    # Save DataFrame as Parquet file
    df.to_parquet('dynamodb-export.parquet', engine='pyarrow')

    # Clean up temporary file
    os.remove('temp.json.gz')

    print("Conversion to Parquet completed successfully.")
else:
    print("No files found in the specified directory.")


# but then I needed to flatten it:

import pandas as pd

# Read the Parquet file
#df = pd.read_parquet('dynamodb-export.parquet', engine='pyarrow')
#this would have written the unflattened file

import json

# Function to extract values from DynamoDB's JSON format
def extract_value(item):
    if isinstance(item, dict) and 'S' in item:
        return item['S']
    return item

# Flatten the nested JSON structure
flattened_data = []

for index, row in df.iterrows():
    item = row['Item']
    flattened_row = {key: extract_value(value) for key, value in item.items()}
    flattened_data.append(flattened_row)

# Create a new DataFrame with the flattened structure
flattened_df = pd.DataFrame(flattened_data)

# Save the flattened DataFrame as a new Parquet file
flattened_df.to_parquet('flattened-dynamodb-export.parquet', engine='pyarrow')

