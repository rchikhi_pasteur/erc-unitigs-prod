#!/usr/bin/env python
# original goal was to see if the same job was run on standard and d instances
# but I had forgotten that I made precautions so that this never happens
# now this is just a way to parse the jobs and recover accessions

import boto3, re,sys

def find_accession_in_logstream(log_stream_name):
    #log_stream_name = 'indextheplanet-8gb-job/default/e8f65c5670a94c2882a6658827950512'
    
    # Initialize a boto3 client for CloudWatch Logs
    client = boto3.client('logs', region_name='us-east-1')
    
    # Define the log group and stream names
    log_group_name = '/aws/batch/job'
    
    # Initialize the next token variable
    next_token = None

    accession = None
    instance_type = None
    
    # Loop to get log events and search for "SRR"
    while True:
        # Get log events
        if next_token:
            response = client.get_log_events(
                logGroupName=log_group_name,
                logStreamName=log_stream_name,
                nextToken=next_token
            )
        else:
            response = client.get_log_events(
                logGroupName=log_group_name,
                logStreamName=log_stream_name
            )
    
        # Search for "SRR" in log events, also instance types
        for event in response['events']:
            msg = event['message']
            if ('ERR' in msg or 'SRR' in msg or 'ERR' in msg):
                #print(f"SRR found in message: {msg}")
                # If you need to break after the first occurrence uncomment the next line
                found_accession = re.findall(r'\b[DES]RR\d+\b', msg)[0]
                if accession is not None and found_accession != accession:
                    #print("this is a multi-accession job, let's skip it")
                    return None, None
                accession = found_accession
            if "instance type -" in msg:
                #print(f"instance type found in message: {msg}")
                instance_type = msg.split()[-1]
                
        if accession is not None and instance_type is not None: break
    
        # Check if the 'nextForwardToken' is the same as the current 'nextToken'
        # which indicates there are no more log events to retrieve
        if next_token == response['nextBackwardToken']:
            #print("Reached the end of the log events.")
            break
        else:
            next_token = response['nextBackwardToken']
    
        # Implement a small delay to avoid hitting AWS API rate limits
        # time.sleep(1) # Uncomment if needed
    return accession, instance_type

import boto3
import threading
from datetime import datetime, timedelta
from concurrent.futures import ThreadPoolExecutor, as_completed

# Initialize the AWS Batch client
batch_client = boto3.client('batch')

jobQueue = "IndexThePlanetJobQueue"

# Function to list jobs with a specific status
def list_jobs(status):
    return batch_client.list_jobs(jobQueue=jobQueue, jobStatus=status)['jobSummaryList']

def chunk_list(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

# Function to describe jobs in chunks
def describe_jobs(job_ids):
    all_jobs = []
    for chunk in chunk_list(job_ids, 100):  # Process in chunks of 100
        response = batch_client.describe_jobs(jobs=chunk)
        all_jobs.extend(response['jobs'])
    return all_jobs
    
log_streams = {}

dynamodb = boto3.resource('dynamodb')
log_streams_lock = threading.Lock()

def process_job(job):
    global log_streams
    if job['status'] == 'FAILED':
        return None
    # see if jobId alread in table
    with log_streams_lock:
        inspection_table = dynamodb.Table("Logan-JobInspection")
        response = inspection_table.get_item(Key={'jobId': job['jobId']}).get('Item')
        if response: return None
    startedAt = datetime.fromtimestamp(int(job['startedAt']) / 1000.0)
    stoppedAt = datetime.fromtimestamp(int(job['stoppedAt']) / 1000.0)
    jobTime = stoppedAt - startedAt

    twenty_minutes = timedelta(minutes=35)
    if jobTime < twenty_minutes:
        return None

    log_stream = get_log_stream_name(job)
    if log_stream:
        accession,instance_type = find_accession_in_logstream(log_stream)
        with log_streams_lock:
            if accession is not None and instance_type is not None:
                log_streams[job['jobId']] = (accession, instance_type, jobTime)
                inspection_table = dynamodb.Table("Logan-JobInspection")
                inspection_table.put_item(Item={
                    'jobId': job['jobId'],
                    'InstanceType': instance_type,
                    'Accession': accession,
                    'JobTime': str(jobTime)  # JobTime is a timedelta, converting to string for storage
                })
                print(".", end="") 
            else:
                print("x", end="")
            sys.stdout.flush()

        
def get_log_streams(jobs):
    for i,job in enumerate(jobs):
        if 'arrayProperties' in job:
            if 'index' in job['arrayProperties']:
                # will be handled by process_job
                pass
            else:
                print(job['jobId'], job['arrayProperties']['statusSummary'])
                # It's a parent job, handle job array
                child_jobs = list_jobs_for_array(job)
                child_job_details = describe_jobs(child_jobs)
                get_log_streams(child_job_details)
                # Parallel processing using ThreadPoolExecutor
                with ThreadPoolExecutor(max_workers=20) as executor:
                    future_to_job = {executor.submit(process_job, job): job for job in child_job_details}
                    for future in as_completed(future_to_job):
                        job = future_to_job[future]
                        try:
                            future.result()
                        except Exception as exc:
                            print('%r generated an exception: %s' % (job, exc))
        else:
            # Skip standalone jobs
            print("Standalone job, skipping")
            continue
            
def get_log_stream_name(job):
    try:
        return job['container']['logStreamName']
    except KeyError:
        return None

def list_jobs_for_array(parent_job):
    child_job_ids = []
    array_size = parent_job['arrayProperties'].get('size', 0)
    parent_job_id = parent_job['jobId']
    for i in range(array_size):
        child_job_id = f"{parent_job_id}:{i}"
        child_job_ids.append(child_job_id)
    return child_job_ids

# List all failed jobs
failed_jobs = list_jobs('FAILED')

# Extract job IDs
failed_job_ids = [job['jobId'] for job in failed_jobs]

# Describe failed jobs to get details
failed_job_details = describe_jobs(failed_job_ids)

get_log_streams(failed_job_details)
