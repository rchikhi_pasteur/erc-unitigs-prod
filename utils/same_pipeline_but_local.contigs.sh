#!/bin/bash
# aws s3 cp s3://sra-pub-run-odp/sra/$accession/$accession . && mv $accession $accession.sra

threads=10

set -e 

function cleanup {
  local exit_code=$?
  local line=${BASH_LINENO[0]}
  local command=${BASH_COMMAND}

  if [ $exit_code -ne 0 ]; then
    echo "run_local_contigs exited abruptly" >> $workdir/error.txt
    echo "Error occurred on or near line $line: $command" >> $workdir/error.txt
  fi
}

trap cleanup EXIT

if [ $# -lt 1 ]; then
   echo 1>&2 "$0: not enough arguments. Needs the hash of the workdir"
   exit 1
fi
ulimit -n 2048

DIR=/pasteur/appa/homes/rchikhi/data/logan/
sradir=$HOME/scratch/logan_sra/
workdir=$HOME/scratch/logan-$1
accession=$(ls "$workdir" | grep -o '[SDE]RR[0-9]*' | head -n 1)

cd $workdir

if [ ! -f "$accession.unitigs.fa" ]; then
    echo "unitigs for accession=$accession should exist, but they don't"
    exit 1
else
    echo "unitigs for $accession already exist, continuing"
fi

if [ -f "$accession.contigs.fa" ]; then
    echo "contigs for $accession already exist in this logan-xxx tempdir, not pursuing"
    exit 1
fi


echo -e ">1\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" > $accession
export LANG=C
#sed -i 's/ka:/km:/g' $accession.unitigs.fa
python $DIR/replace_string.py $accession.unitigs.fa "ka:" "km:"

\time $HOME/tools/minia/build/bin/minia -in $accession -nb-cores $threads -max-memory 2000 \
                               -skip-bcalm -skip-bglue -redo-links

perl -i -pe 'if (/^>/) { s/^>/>${accession}_/; s/km:/ka:/; }' ${accession}.unitigs.fa
perl -i -pe 'if (/^>/) { s/^>/>${accession}_/; s/km:/ka:/; s/LN:i:\S+ //; s/KC:i:\S+ //; }' ${accession}.contigs.fa

\time f2sz -l 13 -b 128M -F -i -f -T $threads $accession.unitigs.fa
\time f2sz -l 13 -b 128M -F -i -f -T $threads $accession.contigs.fa

mv $accession.unitigs.* $DIR/unitigs/
mv $accession.contigs.* $DIR/contigs/
rm -f $accession $accession.h5 
rm -f error.txt # since it went fine.. discard previous error to be able to remove the folder

echo "all done for $accession! deleting $sradir/$accession.sra"
rm -f $sradir/$accession.sra

rmdir $workdir
