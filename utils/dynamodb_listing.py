# this one parses the Logan table and get aggregates
# let's see how quick this is, and how painful in terms of costs
# well, it's not fast. 2 minutes for 2.7M entries

import boto3

# Initialize a DynamoDB client
dynamodb = boto3.resource('dynamodb')

table = dynamodb.Table('Logan')
# Scan the table
scan_kwargs = {
       'ProjectionExpression': 'accession, ARM64_return_value'
       }

# Structure to hold accession counts and instance types
nb_acc = 0
g = open("dynamodb_listing.txt","w")
while True:
    response = table.scan(**scan_kwargs)
    items = response['Items']


    for item in items:
        accession = item['accession']
        if 'ARM64_return_value' not in item: continue
        return_value = int(item['ARM64_return_value'])

        if return_value > 0:
            g.write(f"{accession}\t{return_value}\n")
        nb_acc += 1

    # Check if there are more items to fetch
    if 'LastEvaluatedKey' in response:
        scan_kwargs['ExclusiveStartKey'] = response['LastEvaluatedKey']
    else:
        break  # Exit the loop if no more items

print(nb_acc,"accessions searched")
g.close()
