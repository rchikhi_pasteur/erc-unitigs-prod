groundtruth = {}
for line in open("dynamodb_unitigs_sumlen.subset60k-serratus.txt"):
    acc, size = line.split()
    groundtruth[acc]=int(size)

for line in open("dynamodb_unitigs_sumlen.txt"):
    acc, size = line.split()
    size = int(size)
    if acc in groundtruth:
        groundtruth_size = groundtruth[acc]
        if abs(size-groundtruth_size)/groundtruth_size> 0.01: # 1% unitig size discrepancy
            print("discrepancy",acc,size,"!=",groundtruth[acc])


