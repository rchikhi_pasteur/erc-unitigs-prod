# this is the continuation of the _to_dynamodb script
# but arf this is all pointless, since i've often been careful to not reuse the same accessions
# this was the whole point of taking fresh sets every test run
# yet somehow a few slipped through the cracks, not many though.

import boto3

# Initialize a DynamoDB client
dynamodb = boto3.resource('dynamodb')

# Reference to your table
table = dynamodb.Table('Logan-JobInspection')

# Scan the table
response = table.scan()

# Extracting items
items = response['Items']

# Structure to hold accession counts and instance types
accession_data = {}

for item in items:
    accession = item['Accession']
    instance_type = item['InstanceType']
    job_time = item['JobTime']

    if accession not in accession_data:
        accession_data[accession] = {'count': 0, 'instance_types': set()}

    accession_data[accession]['count'] += 1
    accession_data[accession]['instance_types'].add((instance_type,job_time))

# Filter accessions that were run twice or more and print them with their instance types
for accession, data in accession_data.items():
    if data['count'] >= 2:
        print(f"Accession: {accession}, Count: {data['count']}, Instance Types: {data['instance_types']}")

