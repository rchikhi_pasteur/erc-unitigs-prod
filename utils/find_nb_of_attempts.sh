#!/bin/bash

rm -f find_running_jobs_in_jobarray.txt
for i in $(seq 0 23)
do

	JOB_NAME="cuttlefisha_sra-aarch64-Mar19-2024-120324-run61"
	JOB_QUEUE="IndexThePlanetJobQueueGraviton$i" 

	id=$(aws batch list-jobs --job-status PENDING --job-queue "$JOB_QUEUE" | jq --arg JOB_NAME "$JOB_NAME" -r '.jobSummaryList[] | select(.jobName == $JOB_NAME) | .jobId')

# check there is just one id
# Convert the job IDs into an array
ids=($id)

# Check the number of job IDs
if [ "${#ids[@]}" -eq 1 ]; then
	echo "Found exactly one job ID: ${ids[0]}"
elif [ "${#ids[@]}" -eq 0 ]; then
	echo "No job IDs found for job name: $JOB_NAME"
	continue
else
	echo "Found more than one job ID in job queue: $JOB_QUEUE"
fi


for id in "${ids[@]}"; do

	jobIds=$(aws batch list-jobs --array-job-id "$id" | jq -r '.jobSummaryList[] | select(.status == "RUNNING") | .jobId')


# Loop through each job ID to describe the job and get environment variables
for jobId in $jobIds; do
	echo "Fetching environment variables for Job ID: $jobId"

  	# Use aws batch describe-jobs to get job details
        jobDetails=$(aws batch describe-jobs --jobs ${jobId})
        
        # Extract the number of attempts using jq
        attempts=$(echo $jobDetails | jq '.jobs[].attempts | length')
        echo "Job ID: $jobId has made $attempts attempt(s)."
done

done

done
