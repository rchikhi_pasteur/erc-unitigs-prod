Files:
- run_local.sh is a bastardization of my aws batch pipeline
- to do everything: same_pipeline_but_local.submit.sh calls run_local.sh
- to do just contigs: same_pipeline_but_local.submit_contigs.sh calls run_local_contigs.sh

Make sure to grep for "Segmentation fault", at least one log has it, it's a sneaky fasterq-dump crash that doesnt prevent the rest of the job from running.
