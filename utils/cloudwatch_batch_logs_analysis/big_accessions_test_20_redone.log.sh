\time awslogs get /aws/batch/job ALL -s12h -G --timestamp |zstd -c > big_accessions_test_20_redone.log.zst
# custom because i know there is only 18 in logs
zstdcat big_accessions_test_20_redone.log.zst|grep "accession -"|grep -Fwf ~/erc-unitigs-prod/sets/poc-large-20.txt | tail -n 20 |awk '{print $1}' > big_accessions_test_20_redone.log.jobids.txt
# manual inspection
for jobid in $(awk '{print $1}' big_accessions_test_20_redone.log.jobids.txt); do echo $jobid;zstdcat big_accessions_test_20_redone.log.zst|grep $jobid; done |less

# get unitigs seqstats
for jobid in $(awk '{print $1}' big_accessions_test_20_redone.log.jobids.txt); do echo $jobid;zstdcat big_accessions_test_20_redone.log.zst|grep $jobid|grep "seqkit stats -T -a .*unitigs.fa" -A2; done |less

# get runtimes
for jobid in $(awk '{print $1}' big_accessions_test_20_redone.log.jobids.txt); do echo $jobid;zstdcat big_accessions_test_20_redone.log.zst|grep $jobid|grep "Complete" -A2; done |less
