import zstandard as zstd
import io
import os
import sys
import pickle
from datetime import datetime

#prefix = "."
#which_run = "300d"
prefix = "run6"
which_run = "run6"

def zstd_read_lines(filename):
    counter = 0
    # Open the compressed file
    with open(filename, 'rb') as compressed:
        dctx = zstd.ZstdDecompressor()
        
        # Create a stream reader to decompress the data as it is read
        with dctx.stream_reader(compressed) as reader:
            text_stream = io.TextIOWrapper(reader, encoding='utf-8')
            
            # Read the file line by line
            for line in text_stream:
                if counter > 0 and counter % 1000000 == 0:
                    sys.stderr.write(".")
                    sys.stderr.flush()
                yield line.strip()
                counter += 1
    sys.stderr.write("\n")

def save_serialized_jobids(data, file_path):
    with open(file_path, 'wb') as f:
        pickle.dump(data, f)

def load_serialized_jobids(file_path):
    with open(file_path, 'rb') as f:
        return pickle.load(f)


def parse_jobid(log_file_path):
    d_jobid = dict()
    for line in zstd_read_lines(log_file_path):
        ls = line.split()
        jobid, date_str, acc = ls[0], ls[1], ls[-1]
        time = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S.%fZ")
        if jobid not in d_jobid:
            d_jobid[jobid] = [(acc,time)]
        else:
            d_jobid[jobid].append((acc,time))
    return d_jobid


# do the job ids
serialized_file_path = f'/mnt/cloudwatch_logs/{prefix}/d_jobid.{which_run}.pickle'
if os.path.exists(serialized_file_path):
    # Load the serialized data
    d_jobid = load_serialized_jobids(serialized_file_path)
    print("Loaded serialized jobid,acc,time.")
else:
    # Parse the logs and serialize the data
    log_file_path = f"/mnt/cloudwatch_logs/{prefix}/accession_.{which_run}.log.zst"
    d_jobid = parse_jobid(log_file_path)
    save_serialized_jobids(d_jobid, serialized_file_path)
    print("Parsed and serialized jobid,acc,time.")

# do the sra accession sizes
d_acc_size_avgspotlen = dict()
for line in zstd_read_lines("/home/ec2-user/erc-unitigs-prod/Athena_Dec_10_public.tsv.stillthere.mbases_avgspotlen.zst"):
    ls = line.split()
    acc, size, avgspotlen = ls
    d_acc_size_avgspotlen[acc]=(int(size),int(avgspotlen))
print("Parsed acc, real_size, avgspotlen")
 

# returns accession that was processed by this jobid before a particular time
def determine_accession(jobid, time):
    if jobid not in d_jobid:
        return None
    previous_time = None
    previous_acc = None
    for acc, other_time in d_jobid[jobid]:
        if previous_time is not None and time > previous_time:
            if time < other_time:
                return previous_acc 
        previous_time = other_time
        previous_acc = acc
    if time < previous_time:
        #print("incomplete log, unfortunately")
        return None
    return previous_acc

nb_acc_not_found = 0
nb_acc_disappeared_sra = 0
acc_found = set()
nbreads_discrep = set()
okay = set()
nb_oddities1 = 0
nb_oddities2 = 0
i=open("discrep.txt","w")

d_total_seq_length = {}
for line in zstd_read_lines(f"/mnt/cloudwatch_logs/{prefix}/total_sequence_length.{which_run}.log.zst"):
    ls = line.split()
    jobid, date_str, seqlength = ls[0], ls[1], int(ls[-1][:-1])
    time = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S.%fZ")
    
    acc = determine_accession(jobid, time)
    if acc is None:
        continue
    seqlength /= 1000000
    seqlength = int(seqlength)
    d_total_seq_length[acc]=seqlength
print("Parsed and recorded total_sequence_length")

nb_acc_in_nbseq_but_not_totseqlength =0
for line in zstd_read_lines(f"/mnt/cloudwatch_logs/{prefix}/number_of_sequences.{which_run}.log.zst"):
    ls = line.split()
    jobid, date_str, nbreads_cuttle = ls[0], ls[1], int(ls[-1][:-1])
    time = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S.%fZ")

    acc = determine_accession(jobid, time)
    if acc is None:
        nb_acc_not_found += 1
        continue
    else:
        acc_found.add(acc)

    if acc not in d_acc_size_avgspotlen:
        nb_acc_disappeared_sra += 1
        continue

    if acc not in d_total_seq_length:
        nb_acc_in_nbseq_but_not_totseqlength += 1
        continue

    size, avgspotlen = d_acc_size_avgspotlen[acc]
    estimated_nbreads = (size*1000000.0)/avgspotlen
    if size < 100:
        # let's redo all small accessions using the athena list
        continue
    else:
        # because sometimes, SRA number of spots (paired reads) instead of individual reads
        estimated_nbreads = estimated_nbreads if abs(1.0*(nbreads_cuttle-estimated_nbreads)/estimated_nbreads) < abs(1.0*(nbreads_cuttle//2-estimated_nbreads)/estimated_nbreads) \
                       else estimated_nbreads*2
        metric_nbreads=1.0*(nbreads_cuttle-estimated_nbreads)/estimated_nbreads
        seqlength_cuttle=d_total_seq_length[acc]
        metric_size=1.0*(seqlength_cuttle-size)/size 
        bad=metric_nbreads < -0.001 and metric_size < -0.01
        i.write(f"{'-' if not bad else '!'}acc-size-estimated_nbreads: {acc} {size} {estimated_nbreads} -nbreads-m1-m2 {nbreads_cuttle} {metric_nbreads} -seqlength-m3: {seqlength_cuttle} {metric_size}\n")
        # one of the two m1/m2 might be close to zero, which is a sign of a good estimation, except in super unlucky case where we've read exactly half of reads
        # i'm also confirming with m3 
        if bad:
            nbreads_discrep.add(acc)
            if acc in okay:
                nb_oddities1 += 1
                #print(acc,"is not ok after all?")
        else:
            okay.add(acc)
            if acc in nbreads_discrep:
                nb_oddities2 += 1
                #print(acc,"was size_discrep but is ok now?")

nbreads_discrep_size = 0
for acc in nbreads_discrep:
    size, avgspotlen = d_acc_size_avgspotlen[acc]
    nbreads_discrep_size += size

print(f"{nb_acc_not_found}/{len(acc_found)} accessions not found in accession_.{which_run}.log.zst, {nb_acc_disappeared_sra} disappeared from SRA, {nb_acc_in_nbseq_but_not_totseqlength} in 1/2 logs")
print(f"{len(nbreads_discrep)}/{len(acc_found)} accessions to redo due to nbreads discrep, {nbreads_discrep_size} MB total")
print(f"{nb_oddities1} type 1 oddities, {nb_oddities2} type 2 oddities")

nb_small = 0
nb_small_size = 0
to_redo = set()
for acc in d_acc_size_avgspotlen:
    size, avgspotlen = d_acc_size_avgspotlen[acc]
    if size < 100:
        to_redo.add(acc)
        nb_small += 1
        nb_small_size += size
print(nb_small,"small accessions to redo,", nb_small_size, "MBases")

g = open(f"determine_corrupt_accessions.{which_run}.redo.txt","w")
for acc in to_redo | nbreads_discrep:
    g.write(acc+"\n")

h = open(f"determine_corrupt_accessions.{which_run}.okay.txt","w")
for acc in okay:
    h.write(acc+"\n")

h.close()
g.close()

