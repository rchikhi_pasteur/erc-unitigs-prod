\time awslogs get /aws/batch/job ALL -s3d -G --timestamp |zstd -c > big_accessions_test_20.log.zst
# custom because i know there is only 18 in logs
zstdcat big_accessions_test_20.log.zst|grep "accession -"|grep -Fwf ~/erc-unitigs-prod/sets/poc-large-20.txt | tail -n 18 |awk '{print $1}' > big_accessions_test_20.log.jobids.txt
# manual inspection
for jobid in $(awk '{print $1}' big_accessions_test_20.log.jobids.txt); do echo $jobid;zstdcat big_accessions_test_20.log.zst|grep $jobid; done

for jobid in $(awk '{print $1}' big_accessions_test_20.log.jobids.txt); do echo $jobid;zstdcat big_accessions_test_20.log.zst|grep $jobid|grep "seqkit stats -T -a .*unitigs.fa" -A2; done
