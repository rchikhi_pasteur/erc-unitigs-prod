#!/bin/bash

# Bucket name
BUCKET_NAME="logan-refseq-mirror"

# Interval in seconds between measurements (e.g., 1 second)
INTERVAL=1

# Function to get total size in MB of all files in the bucket
get_total_size_mb() {
    aws s3 ls s3://$BUCKET_NAME --recursive --summarize | awk '/Total Size:/{print $3/1024/1024}'
}

# Initial size
initial_size=$(get_total_size_mb)

# Watch and calculate upload progress
while true; do
    sleep $INTERVAL
    new_size=$(get_total_size_mb)
    diff_size=$(echo "$new_size - $initial_size" | bc)
    mb_per_sec=$(echo "$diff_size / $INTERVAL" | bc)
    echo "$mb_per_sec MB/s"
    initial_size=$new_size
done

