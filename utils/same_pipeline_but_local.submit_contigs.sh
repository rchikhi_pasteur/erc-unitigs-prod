#!/bin/bash

# Directory containing .sra files
DIRECTORY="$HOME/scratch/"

# Iterate over each item in the scratch directory
for dir in "$DIRECTORY"*/ ; do
    # Check if the directory name matches the pattern "logan-xxxx"
    if [[ $(basename "$dir") == logan-* ]]; then
        # Extract the "xxxx" part and output it
        logandir=$(basename "$dir" | sed 's/logan-//')

        if ls "$DIRECTORY/$logandir"/*.contigs.fa 1> /dev/null 2>&1; then
            echo "contigs already exist in this logan-xxx tempdir, not pursuing"
            continue
        fi

        # Submit a job for this file
        sbatch  -o $PWD/slurm-logs/slurm-%j.out -e $PWD/slurm-logs/slurm-%j.err -q seqbio -p seqbio --mem 700G -c 10 $PWD/same_pipeline_but_local.contigs.sh "$logandir"
	fi
done 

