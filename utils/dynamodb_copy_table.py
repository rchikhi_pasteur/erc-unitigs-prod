#more efficient than one by one

#!/usr/bin/env python
import boto3
from botocore.exceptions import ClientError
import sys
import os

if len(sys.argv) != 3:
    print("Usage: %s <source_table_name> <destination_table_name>" % sys.argv[0])
    sys.exit(1)

src_table_name = sys.argv[1]
dst_table_name = sys.argv[2]
region = os.getenv('AWS_DEFAULT_REGION', 'us-east-1')

dynamodb = boto3.resource('dynamodb', region_name=region)
src_table = dynamodb.Table(src_table_name)
dst_table = dynamodb.Table(dst_table_name)

# Read and copy the target table to be copied
try:
    src_table.load()
except ClientError as e:
    if e.response['Error']['Code'] == 'ResourceNotFoundException':
        print("Table %s does not exist" % src_table_name)
        sys.exit(1)
    else:
        raise

print("*** Reading key schema from %s table" % src_table_name)
src_key_schema = src_table.key_schema
hash_key = ''
range_key = ''
for schema in src_key_schema:
    attr_name = schema['AttributeName']
    key_type = schema['KeyType']
    print("schema:", attr_name, key_type)
    if key_type == 'HASH':
        hash_key = attr_name
    elif key_type == 'RANGE':
        range_key = attr_name

def scan_and_process_items():
    scan_kwargs = {}
    while True:
        response = src_table.scan(**scan_kwargs)
        items = response.get('Items', [])
        process_items(items)
        last_evaluated_key = response.get('LastEvaluatedKey')
        if not last_evaluated_key:
            break
        scan_kwargs['ExclusiveStartKey'] = last_evaluated_key

def process_items(items):
    batch_items = []
    for item in items:
        new_item = {}
        new_item[hash_key] = item[hash_key]
        if range_key:
            new_item[range_key] = item[range_key]
        for f in item.keys():
            if f in [hash_key, range_key]:
                continue
            if not ("seqstats_" in f or "_compression" in f):
                continue
            new_item[f] = item[f]
        batch_items.append({'PutRequest': {'Item': new_item}})
        if len(batch_items) == 50:
            try:
                with dst_table.batch_writer() as batch:
                    for bi in batch_items:
                        batch.put_item(Item=bi['PutRequest']['Item'])
                batch_items = []
            except ClientError as e:
                print(f"Error writing batch to {dst_table_name}: {e}")
                sys.exit(1)
    # Write any remaining items
    if batch_items:
        try:
            with dst_table.batch_writer() as batch:
                for bi in batch_items:
                    batch.put_item(Item=bi['PutRequest']['Item'])
        except ClientError as e:
            print(f"Error writing batch to {dst_table_name}: {e}")
            sys.exit(1)

print("*** Starting scan of %s table" % src_table_name)
scan_and_process_items()

print("We are done. Exiting...")

