import boto3
import time
from botocore.exceptions import ClientError

# Initialize the EC2 client
ec2 = boto3.client('ec2')

PAGE_SIZE = 1000
PREFIX_NAME_INSTANCES ="IndexThePlanet-Unitigs-SPOT"

def build_describe_params(values, max_results, next_token=None):
    params = {
        "Filters": [
            {
                'Name': 'tag:Name',
                'Values': values
            },
            {
                'Name': 'instance-state-name',
                'Values': ['running']  # Adjust as needed for desired states
            },
        ],
        "MaxResults": max_results
    }
    if next_token:
        params["NextToken"] = next_token
    return params

def describe_instances(values:list, max_results:int, next_token=None):
    instances_without_block_devices = []
    params = build_describe_params(values, max_results, next_token)
    try:
        response = ec2.describe_instances(**params)
        # Iterate through reservations and instances
        for reservation in response['Reservations']:
            for instance in reservation['Instances']:
                # Check if BlockDeviceMappings is empty
                nb_ebs=len(instance.get('BlockDeviceMappings'))
                if nb_ebs < 5:
                    print("ebs problem",instance['InstanceId'],len(instance.get('BlockDeviceMappings')))
                else:
                    block_devices = instance.get('BlockDeviceMappings', [])
                    print(block_devices)
                    if any(ebs['Ebs']['VolumeSize'] > 100 for ebs in block_devices if 'Ebs' in ebs):
                        print("problematic instance?", instance['InstanceId'])

    except ClientError as e:
        print("Exception with describe_instances", e)
        time.sleep(1)
    return instances_without_block_devices, response

# Example usage:
if __name__ == '__main__':
    ibd, response = describe_instances([f"{PREFIX_NAME_INSTANCES}*"], PAGE_SIZE, next_token=None)

    while "NextToken" in response:
        ibd, response = describe_instances([f"{PREFIX_NAME_INSTANCES}*"], PAGE_SIZE, next_token=response['NextToken'])
        print("queried instances IDs")

