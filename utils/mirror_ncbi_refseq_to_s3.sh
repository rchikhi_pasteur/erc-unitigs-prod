#!/bin/bash
#wget https://ftp.ncbi.nlm.nih.gov/sra/refseq/refseq.dir
#mv refseq.dir mirror_ncbi_refseq_to_s3.refseq.dir

#single threaded version
#for name in $(cat mirror_ncbi_refseq_to_s3.refseq.dir) 
#do
#	echo $name
#	wget -qO- "https://ftp.ncbi.nlm.nih.gov/sra/refseq/$name" | aws s3 cp - s3://logan-refseq-mirror/$name
#done

N=10
cut -d"|" -f1 mirror_ncbi_refseq_to_s3.refseq.dir | parallel -j $N echo {} ';' wget -qO- https://ftp.ncbi.nlm.nih.gov/sra/refseq/{} '|' aws s3 cp - s3://logan-refseq-mirror/{}/{}

