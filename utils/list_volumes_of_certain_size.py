import boto3
import time
from datetime import datetime, timezone
from botocore.exceptions import ClientError

ec2 = boto3.client('ec2')

s=set()
def describe_volumes_with_specific_size(size_gb, max_results, next_token=None):
    volumes_with_specific_size = []
    try:
        params = {
            "MaxResults": max_results
        }
        if next_token:
            params["NextToken"] = next_token

        response = ec2.describe_volumes(**params)
        for volume in response['Volumes']:
            volume_creation_time = volume['CreateTime'].replace(tzinfo=timezone.utc)
            if (datetime.now(timezone.utc) - volume_creation_time).total_seconds() < 3600:
                continue
            if volume['Size'] > size_gb-30 and volume['Size'] < size_gb+30:
                for attachment in volume['Attachments']:
                    print("Volume with 100 GB:", volume['VolumeId'], "attached to instance", attachment['InstanceId'])
                    volumes_with_specific_size.append((volume['VolumeId'], attachment['InstanceId']))
                    s.add(attachment['InstanceId'])

    except ClientError as e:
        print("Exception with describe_volumes", e)
        time.sleep(1)
    return volumes_with_specific_size, response

# Example usage:
if __name__ == '__main__':
    MAX_RESULTS=1000
    volumes, response = describe_volumes_with_specific_size(100, MAX_RESULTS, next_token=None)

    while "NextToken" in response:
        more_volumes, response = describe_volumes_with_specific_size(100, MAX_RESULTS, next_token=response['NextToken'])
        volumes.extend(more_volumes)
        print("Queried more volumes")

    for volume_id, instance_id in volumes:
        print(f"Volume {volume_id} is 100 GB and attached to instance {instance_id}")

    g=open("instances_to_kill.txt","w")
    for ins in s:
        g.write(ins+"\n")
    g.close()
