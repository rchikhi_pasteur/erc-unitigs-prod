#!/bin/bash
#set -x
for acc in $(cat ../sets/170524-extra.tsv | awk '{print $1}')
do
	for which in contigs unitigs 
	do
		aws s3 cp s3://logan-extra/${which:0:1}/$acc/$acc.$which.fa.zst s3://logan-pub/${which:0:1}/$acc/
	done
done
