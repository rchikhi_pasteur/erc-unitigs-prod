#!/bin/bash

# typical input: ../sets/poc-subset.txt


# Check if input file is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <input_file>"
    exit 1
fi

input_file="$1"

# Check if input file exists
if [ ! -f "$input_file" ]; then
    echo "Input file not found: $input_file"
    exit 1
fi

# Process each SRA accession
while IFS= read -r accession
do
    # Check if accession is empty
    if [ -z "$accession" ]; then
        continue
    fi

    echo "Processing $accession..."

    aws s3 cp s3://sra-pub-run-odp/sra/$accession/$accession .
    mv $accession $accession.sra

    # Get the size of the input SRA file in megabytes
    sra_file_size=$(du -m "$accession.sra" | cut -f1)

    # Skip files smaller than 100 MB
    if [ "$sra_file_size" -lt 100 ]; then
        echo "Skipping $accession: File size ($sra_file_size MB) is smaller than 100 MB"
        continue
    fi

    # Time the fasterq-dump command
    start_time=$(date +%s.%N)
    fasterq-dump --fasta-unsorted --seq-defline '>' --stdout "$accession.sra" > /dev/null
    end_time=$(date +%s.%N)
    duration=$(echo "$end_time - $start_time" | bc)

    # Calculate throughput in MB/s
    throughput=$(echo "scale=2; $sra_file_size / $duration" | bc)

    echo "Time taken: $duration seconds"
    echo "Throughput: $throughput MB/s"
    echo "File size: $sra_file_size MB"
    rm -f $accession.sra

done < "$input_file"

