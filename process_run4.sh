#!/bin/bash
bucket=logan-canada

echo "Are you sure those jobs are fresh?"
read -p "Will $(tput bold)start the BIG run$(tput sgr0) to s3://$(tput bold)$bucket$(tput sgr0). Nothing will be erased from that bucket  Proceed? (yes/no) " response

case "$response" in
    [yY][eE][sS]|[yY])
        # Continue with the rest of the script
        ;;
    *)
        echo "Exiting the script."
        exit 1
        ;;
esac


echo "220224-run4-vague02"> set
bash process_array.sh $bucket IndexThePlanetJobQueueGraviton
