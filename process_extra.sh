#!/bin/bash
bucket=logan-extra

read -p "Will $(tput bold)start a run$(tput sgr0) to s3://$(tput bold)$bucket$(tput sgr0). Nothing will be erased from that bucket  Proceed? (yes/no) " response

case "$response" in
    [yY][eE][sS]|[yY])
        # Continue with the rest of the script
        ;;
    *)
        echo "Exiting the script."
        exit 1
        ;;
esac


#echo "170524-extra"> set
echo "041024-extra" > set #1000G_2504_high_coverage.sequence.index.missing.txt
# status: need to finish it. Some of the samples were not assembled. I've moved the successfully assembled to logan-pub
bash process_array.sh $bucket IndexThePlanetJobQueueGraviton
