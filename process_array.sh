#!/bin/bash -e

outputbucket=$1
jobqueue=$2
dryrun=$3

# Check if an argument is provided
if [ $# -lt 2 ]; then
    echo "Missing arguments, need \$1 for bucket and \$2 for jobqueue. Exiting."
    exit 1
fi

if [ -n "$dryrun" ]; then
	echo "this is a dry run"
fi

# stores the job array .txt files
arraybucket=$(if [[ -z $(aws sts get-caller-identity |grep serratus-rayan) ]]; then echo "logan-dec2023-testbucket"; else echo "logan-testing-march2024"; fi)
arrayfolder=indextheplanet-jobarray
nbCE=1

set=$(cat set)
date=$(date +"%b%d-%Y")
arch=$(uname -m)
tag=cuttlefisha_sra-$arch-$date-$set

rm -f array_8gb.txt array_15gb.txt array_31gb.txt array_63gb.txt

# Execute helper grouping script and save outputs to separate files based on accession_size
python3 group_accessions.py sets/$set.tsv| while IFS=$'\t' read -r accessions size; do
	if (( size < 20000 )); then
		echo "$accessions" >> array_8gb.txt
	elif (( size >= 20000 )) && (( size < 60000 )); then
		echo "$accessions" >> array_15gb.txt
	elif (( size >= 60000 )) && (( size < 180000 )); then
		echo "$accessions" >> array_31gb.txt
	else
		echo "$accessions" >> array_63gb.txt
	fi
done

# Upload files to S3 with a unique identifier (e.g., timestamp)
timestamp=$(date +"%Y%m%d%H%M%S")_$(cat set)_$$_$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13)

split_and_upload() {
    file=$1
    size=$2
    job_name=$3
    default_size=$4
    threads=$5
    jobqueue=$6
    dryrun=$7
    
	# Split the file and upload each part
	split -d -n l/$nbCE $file array_${size}_part_
	nb_parts=$(ls array_${size}_part_*  2>/dev/null | wc -l)
	if [ "$nb_parts" -gt $nbCE ]; then
		echo "warning! more array jobs ($nb_parts) than the number of queues/CEs ($nbCE)"
		exit 1
	fi
	for part in array_${size}_part_*; do
	    part_lines=$(wc -l < $part)
	    if [ "$part_lines" -gt 10000 ]; then
		echo "warning! array job ($part) has more jobs ($part_lines) than allowed (10000)"
		exit 1
	    fi
	done
	for part in array_${size}_part_*; do
	    part_lines=$(wc -l < $part)
	    suffix="${part##*_}"
	    # proper suffix for the job queue
	    suffix=$((10#$suffix))
	    jq=$jobqueue$suffix
	    #jq=${jobqueue}0 # forces to go to job queue 0
	    if [ -n "$dryrun" ]; then
		echo "dry run, not executing array_submit_job for $part to job queue $jq"
	    else
		 if [ "$part_lines" -eq 0 ]; then
			 :
		 elif [ "$part_lines" -eq 1 ]; then
			#Special case: only 1 accession in the file, this must be a small run
			accession=$(cat $part)
			echo "Special case, only 1 accession in the ${size} queue: $accession"
			python3 batch/simple_submit_job.py $accession $default_size $tag $outputbucket $suffix
			echo "single job submitted! ($accession)"
		 else
			s3file=s3://$arraybucket/$arrayfolder/$part"_"$timestamp
			aws s3 cp $part $s3file
			python3 batch/array_submit_job.py $part_lines $tag $s3file $job_name $jq $threads $outputbucket
			echo "array job submitted! ($s3file)"
		fi
	    fi
	done
}

EightGigsJob=$(if      [[ $(uname -m) == "aarch64" ]]; then echo "logan-8gb-jobg";  else echo "indextheplanet-8gb-job"; fi)
FifteenGigsJob=$(if    [[ $(uname -m) == "aarch64" ]]; then echo "logan-15gb-jobg"; else echo "indextheplanet-15gb-job"; fi)
ThirtyOneGigsJob=$(if  [[ $(uname -m) == "aarch64" ]]; then echo "logan-31gb-jobg"; else echo "indextheplanet-31gb-job"; fi)
SixtyThreeGigsJob=$(if [[ $(uname -m) == "aarch64" ]]; then echo "logan-63gb-jobg"; else echo "indextheplanet-63gb-job"; fi)


# submit job arrays
echo "Submitting to JobQueue: $jobqueue"
[ -f array_63gb.txt ] && split_and_upload array_63gb.txt 63gb "$SixtyThreeGigsJob" 400000 32 "$jobqueue" "$dryrun"

# standard:
[ -f array_31gb.txt ] && split_and_upload array_31gb.txt 31gb "$ThirtyOneGigsJob"  150000 16 "$jobqueue" "$dryrun"
# upgrade
#[ -f array_31gb.txt ] && split_and_upload array_31gb.txt 31gb "$SixtyThreeGigsJob"  400000 32 "$jobqueue" "$dryrun"

# standard:
[ -f array_15gb.txt ] && split_and_upload array_15gb.txt 15gb "$FifteenGigsJob"     30000  8 "$jobqueue" "$dryrun"
[ -f array_8gb.txt  ] && split_and_upload array_8gb.txt  8gb  "$EightGigsJob"        1000  4 "$jobqueue" "$dryrun"
# upgrade:
#[ -f array_15gb.txt ] && split_and_upload array_15gb.txt 15gb "$ThirtyOneGigsJob"   150000 16 "$jobqueue" "$dryrun"
#[ -f array_8gb.txt  ] && split_and_upload array_8gb.txt  8gb "$FifteenGigsJob"      30000  8 "$jobqueue" "$dryrun"



#echo "warning: not submitting the long jobs, that's for a later time"

if [ -n "$dryrun" ]; then
	echo "not deleting array jobs for dryrun. to do it, type: rm -f array_8gb_* array_15gb_* array_31gb_* array_63gb_*"
else
	rm -f array_8gb.txt array_15gb.txt array_31gb.txt array_63gb.txt
	rm -f array_8gb_part_* array_15gb_part_* array_31gb_part_* array_63gb_part_*
fi

