export what=unitigs
export what=contigs
echo "statistics on $what"

set=$(cat set)

bucket=logan-dec2023-testbucket
echo "bucket=$bucket"

output=post_1_find_non_unitigged

process_subset() {
    set=$1
    bucket=$2
    what=$3

    awk '{print $1}' sets/$set.tsv  |grep -v Run |grep -v "acc"|sort > sets/$set.txt
    targetlist=sets/$set.txt
    folder=s3://$bucket/${what:0:1}/
    aws s3 ls $folder --recursive |grep $what.fa |awk '{print $4}' |cut -d"/" -f2|cut -d. -f1 |sort |grep -Fwf sets/$set.txt > $output.$set.unitigged.txt
    diff $targetlist $output.$set.unitigged.txt | grep "<" |awk '{print $2}' >> $output.$set.txt
	rm -f $output.$set.unitigged.txt
    echo "set: $set"
}
export -f process_subset

process_subset $set $bucket $what

cat $output.*.txt > $output.txt
rm -f $output.*.txt

echo "$(wc -l $output.txt | awk '{print $1}') $what accessions not present. See list in $output.txt"


# some stats:

# Step 1: Read the missing accessions into an associative array
declare -A missing_accessions
while read -r accession; do
    missing_accessions["$accession"]=1
done < $output.txt

# Variables to keep count of the number of missing and total accessions in each size category
count_lt_10000=0
count_10000_50000=0
count_50000_100000=0
count_ge_100000=0

total_lt_10000=0
total_10000_50000=0
total_50000_100000=0
total_ge_100000=0

# Step 2: Iterate over the files in subsets/*.tsv
file=sets/$set.tsv
    # Step 3: Read each line of the file
    while IFS=$'\t' read -r accession size; do
        # Categorize the accession based on its size and update total counts
        if (( size < 10000 )); then
            ((total_lt_10000++))
        elif (( size < 50000 )); then
            ((total_10000_50000++))
        elif (( size < 100000 )); then
            ((total_50000_100000++))
        else
            ((total_ge_100000++))
        fi

        # Check if the accession is in the list of missing accessions and update missing counts
        if [[ ${missing_accessions["$accession"]} ]]; then
            if (( size < 10000 )); then
                ((count_lt_10000++))
            elif (( size < 50000 )); then
                ((count_10000_50000++))
            elif (( size < 100000 )); then
                ((count_50000_100000++))
            else
                ((count_ge_100000++))
            fi
        fi
    done < "$file"

# Step 4: Calculate and print the percentages and total counts
echo "Percentage of missing $what accessions by size:"
echo "Size < 10000: $(( 100 * count_lt_10000 / total_lt_10000 ))% ($count_lt_10000/$total_lt_10000)"
echo "Size >= 10000 and < 50000: $(( 100 * count_10000_50000 / total_10000_50000 ))% ($count_10000_50000/$total_10000_50000)"
echo "Size >= 50000 and < 100000: $(( 100 * count_50000_100000 / total_50000_100000 ))% ($count_50000_100000/$total_50000_100000)"
echo "Size >= 100000: $(( 100 * count_ge_100000 / total_ge_100000 ))% ($count_ge_100000/$total_ge_100000)"

echo "To investigate the sizes, type: grep -Fwf $output.txt sets/$set.tsv |sort -n -k2 | column -t"
