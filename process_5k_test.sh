#!/bin/bash
bucket=logan-dec2023-testbucket
jobqueue=$(if   [[ $(uname -m) == "aarch64" ]]; then echo "IndexThePlanetJobQueueGraviton";  else echo "IndexThePlanetJobQueue"; fi)
#jobqueue=IndexThePlanetJobQueueDisques


read -p "Will $(tput bold)erase$(tput sgr0) s3://$(tput bold)$bucket$(tput sgr0) folders u/ and c/ before continuing. (yes/no) " response

case "$response" in
    [yY][eE][sS]|[yY])
        # Continue with the rest of the script
        ;;
    *)
        echo "Exiting the script."
        exit 1
        ;;
esac

aws s3 rm s3://$bucket/u/ --recursive
aws s3 rm s3://$bucket/c/ --recursive
echo "poc-subset"> set
bash process_array.sh $bucket $jobqueue
