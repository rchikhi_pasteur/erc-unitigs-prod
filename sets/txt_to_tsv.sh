# $1 = accessions.txt
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
zstdcat $SCRIPT_DIR/../Athena_Dec_10_public.csv.zst | grep -Fwf $1 | awk -F, '{gsub(/"/, "", $1); gsub(/"/, "", $2); print $1"\t"$2}' |grep -v mbases 

