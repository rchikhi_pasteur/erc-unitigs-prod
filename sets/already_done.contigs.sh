s5cmd ls s3://logan-staging/c/ | awk '{print $4}' | cut -d"." -f1 > already_done.contigs.txt
s5cmd ls s3://logan-canada/c/ |grep -oE '\b[A-Z]{3}[0-9]+\b'>> already_done.contigs.txt
sort already_done.contigs.txt | uniq > already_done.contigs.tmp
mv already_done.contigs.tmp already_done.contigs.txt
bash  txt_to_tsv.sh already_done.contigs.txt > already_done.contigs.tsv
