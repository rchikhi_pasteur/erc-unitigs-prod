#!/bin/bash

me=$(basename "$0")
me="${me%.*}"

#https://stackoverflow.com/questions/5914513/shuffling-lines-of-a-file-with-a-fixed-seed
get_seeded_random()
{
  seed="$1"
  openssl enc -aes-256-ctr -pass pass:"$seed" -nosalt \
    </dev/zero 2>/dev/null
}

echo "Make sure you run already-done.sh beforehand!"
zstdcat ../Athena_Dec_10_public.csv.zst | awk -F, '{gsub(/"/, "", $1); gsub(/"/, "", $2); print $1"\t"$2}' | grep -v mbases | shuf --random-source=<(get_seeded_random 42)  | awk '$2 >= 60000 && $2 <= 400000 {print}' > 120324-run61.tsv
awk '{print $1}' 120324-run61.tsv > 120324-run61.txt

# also put small accessions that need to be redone (never done ones, and the recently cleaned-up up ones)
# some weird formatting error, there is an empty line and a NextSeq in the zst..
zstdcat ../Athena_Dec_10_public.csv.zst | awk -F, '{gsub(/"/, "", $1); gsub(/"/, "", $2); print $1"\t"$2}' | grep -vFwf already_done.txt | grep -v mbases |grep -v NextSeq| shuf --random-source=<(get_seeded_random 42)  | awk '$2 < 60000 && $2 != "" {print}' > 120324-run62.tsv
awk '{print $1}' 120324-run62.tsv > 120324-run62.txt


echo "This is a special two-for-one: run 6.1 is the large accessions, run 6.2 is the small ones (divided in vagues)"

