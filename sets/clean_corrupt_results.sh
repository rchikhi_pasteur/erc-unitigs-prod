
read -p "Will $(tput bold)delete corrupt results from logan-canada, the real output bucket$(tput sgr0).   Proceed? (yes/no) " response

read -p "are you super sure, because this is a big haircut (yes/no) " response

case "$response" in
    [yY][eE][sS]|[yY])
        # Continue with the rest of the script
        ;;
    *)
        echo "Exiting the script."
        exit 1
        ;;
esac


bucketName="logan-canada"
bucketName2="logan-staging"
inputFile="clean_corrupt_results.determine_corrupt_accessions.redo.march18.txt"
N=500

# Splitting the input file into batches of 1000 lines each
split -l $N "$inputFile" batch_

# Function to format lines into JSON and call aws s3api delete-objects
processBatch() {
    batchFile=$1
    # Preparing the JSON payload
    jsonPayload="{\"Objects\":["
    while IFS= read -r line; do
	#pathline=u/$line/$line.unitigs.fa.zst
	pathline=u/$line/$line.unitigs.fa.zst
        jsonPayload+="{\"Key\":\"$pathline\"},"
	#pathline=c/$line/$line.contigs.fa.zst
	pathline=c/$line/$line.contigs.fa.zst
        jsonPayload+="{\"Key\":\"$pathline\"},"
    done < "$batchFile"
    jsonPayload="${jsonPayload%,}]}"

    # Calling the AWS CLI to delete objects
    aws s3api delete-objects --bucket "$bucketName" --delete "$jsonPayload" >/dev/null
    aws s3api delete-objects --bucket "$bucketName2" --delete "$jsonPayload" >/dev/null

    # Preparing the JSON payload
    jsonPayload="{\"Objects\":["
    while IFS= read -r line; do
	#pathline=u/$line/$line.unitigs.fa.zst
	pathline=u/$line.unitigs.fa.zst
        jsonPayload+="{\"Key\":\"$pathline\"},"
	#pathline=c/$line/$line.contigs.fa.zst
	pathline=c/$line.contigs.fa.zst
        jsonPayload+="{\"Key\":\"$pathline\"},"
    done < "$batchFile"
    jsonPayload="${jsonPayload%,}]}"

    # Calling the AWS CLI to delete objects
    aws s3api delete-objects --bucket "$bucketName" --delete "$jsonPayload" >/dev/null
    aws s3api delete-objects --bucket "$bucketName2" --delete "$jsonPayload" >/dev/null

    echo "batch $batchFile done"
}

export -f processBatch
export bucketName
export bucketName2

# Find all batch files and use GNU parallel to process them
find . -name 'batch_*' | parallel -j 15 processBatch

# Cleanup batch files after deletion
find . -name 'batch_*' -delete
