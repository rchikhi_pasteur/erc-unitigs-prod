import boto3
import concurrent.futures

# Initialize the S3 client
s3_client = boto3.client('s3')
bucket_name = 'logan-staging'
folder="u"

# Define the prefixes to search for
prefixes = [f'{folder}/{base}{i}' for base in ['DRR', 'SRR', 'ERR'] for i in range(10)]


def list_files_for_prefix(bucket, prefix):
    """List files in S3 for a given prefix."""
    paginator = s3_client.get_paginator('list_objects_v2')
    size = 0
    for page in paginator.paginate(Bucket=bucket, Prefix=prefix):
        for obj in page.get('Contents', []):
            # Sum the sizes of files for this prefix
            size += obj['Size']
    return size

def main():
    total_size = 0
    with concurrent.futures.ProcessPoolExecutor(max_workers=len(prefixes)) as executor:
        # Launch parallel listings for each prefix
        future_to_prefix = {executor.submit(list_files_for_prefix, bucket_name, prefix): prefix for prefix in prefixes}
        
        for future in concurrent.futures.as_completed(future_to_prefix):
            prefix_size = future.result()
            total_size += prefix_size
    
    print(f"Total size of SRRxx/DRRxx/ERRxx files: {total_size} bytes")

if __name__ == "__main__":
    main()
