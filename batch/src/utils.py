import subprocess
import time
import threading
import os
import boto3
import botocore
import sys
import decimal
from datetime import datetime
from boto3.s3.transfer import TransferConfig
from botocore.exceptions import BotoCoreError, ClientError
from random import randint


# check if a file exists
def s3_file_exists(s3, bucket, prefix):
    res = s3.list_objects_v2(Bucket=bucket, Prefix=prefix, MaxKeys=1)
    return 'Contents' in res

def s3_make_unitigs_path(tigs_filename):
    accession=tigs_filename.split('.')[0]
    return "u/"+accession+"/"+tigs_filename

def s3_make_contigs_path(tigs_filename):
    accession=tigs_filename.split('.')[0]
    return "c/"+accession+"/"+tigs_filename

def s3_upload_file(tigs_filename, outputBucket, path, max_attempts=5):
    GB = 1024 ** 3
    MB = 1024 ** 2
    config = TransferConfig(multipart_threshold=256*MB,multipart_chunksize=64*MB,max_concurrency=6)

    s3 = boto3.client('s3') # recreate the client, who knows, possibly the previous one timeout'd?
    for attempt in range(1, max_attempts + 1):
        try:
            # Attempt to upload the file
            s3.upload_file(tigs_filename, outputBucket, path, Config=config)
            print(f"Upload of s3://{outputBucket}/{path} successful after {attempt} {'attempt' if attempt == 1 else 'attempts'}", flush=True)
            break  # If upload succeeds, exit the loop
        except (BotoCoreError, ClientError) as e:
            logging.error(f"Attempt {attempt} failed with error: {e}")
            if attempt < max_attempts:
                # Calculate sleep time in seconds using exponential backoff with jitter
                sleep_time = min(2 ** attempt, 60) + (random.random() * 0.1)  # Adding jitter
                print(f"Waiting {sleep_time:.2f} seconds before retrying...", flush=True)
                time.sleep(sleep_time)
            else:
                print("Max attempts reached. Upload failed.", flush=True)
                raise  # Re-raise the last exception

# ddb logging parameters
table_name = "Logan-recompute"

def update_item_with_retry(table, key, item_name, update_expr, attr_vals, max_retries=3):
    """
    Attempts to update an item in a DynamoDB table with exponential backoff.

    :param table: The DynamoDB table resource.
    :param item_name: The name of the item to update.
    :param name: The attribute name to update.
    :param value: The new value for the attribute.
    :param max_retries: Maximum number of retry attempts. Default is 5.
    :return: True if update succeeds, False otherwise.
    """
    retry_count = 0
    while retry_count < max_retries:
        try:
            # Put item into table
            response = table.update_item(
                Key={
                    key: item_name
                },
                UpdateExpression=update_expr,
                ExpressionAttributeValues=attr_vals,
            )

            # Check for successful response
            if response['ResponseMetadata']['HTTPStatusCode'] == 200:
                return True
            else:
                print("DynamoDB log error:", response['ResponseMetadata']['HTTPStatusCode'], flush=True)
                return False

        except ClientError as e:
            if 'Exceeded' in e.response['Error']['Code']:
                # Calculate wait time in seconds
                wait_time = 2 ** retry_count

                print(f"DynamoDB put_item throughput error, retrying in {wait_time} seconds...", flush=True)
                time.sleep(wait_time)

                # Increment the retry counter
                retry_count += 1
            else:
                # For other exceptions, print the error message and exit the loop
                print("DynamoDB put_item error:", e.response['Error']['Message'], 'table_name', table.name, 'item_name', item_name, flush=True)
                return False
    return False  # Return False if all retries fail


def ddb_log(
        item_name, name, value,
        region='us-east-1'
    ):
    """
    Insert a single record to DynamoDB table.
    PARAMS:
    @table_name: Name of the DynamoDB table
    @item_name: unique string for this record (Primary Key).
    @name, @value: attributes to be inserted.
    """
    # Initialize a session using Amazon DynamoDB
    session = boto3.Session(
        region_name=region
    )

    # Initialize DynamoDB resource
    dynamodb = session.resource('dynamodb')
   
    # Select your DynamoDB table
    table = dynamodb.Table(table_name)
   
    response = update_item_with_retry(table, 'accession', item_name, f"SET {name} = :val", {
                ':val': str(value)
                })
 
    return response

def ddb_log3(
        item_name, name, value, name2, value2, name3, value3,
        region='us-east-1'
    ):
    session = boto3.Session(
        region_name=region
    )
    dynamodb = session.resource('dynamodb')
    table = dynamodb.Table(table_name)
    
    response = update_item_with_retry(table, 'accession', item_name, f"SET {name} = :val, {name2} = :val2, {name3} = :val3", {
                ':val': str(value),
                ':val2': str(value2),
                ':val3': str(value3)
                })
    return response

def ddb_log4(
        item_name, name, value, name2, value2, name3, value3, name4, value4,
        region='us-east-1'
    ):
    session = boto3.Session(
        region_name=region
    )
    dynamodb = session.resource('dynamodb')
    table = dynamodb.Table(table_name)
    
    response = update_item_with_retry(table, 'accession', item_name, f"SET {name} = :val, {name2} = :val2, {name3} = :val3, {name4} = :val4", {
                ':val': str(value),
                ':val2': str(value2),
                ':val3': str(value3),
                ':val4': str(value4)
                })
    return response

def ddb_log5(
        item_name, name, value, name2, value2, name3, value3, name4, value4, name5, value5,
        region='us-east-1'
    ):
    session = boto3.Session(
        region_name=region
    )
    dynamodb = session.resource('dynamodb')
    table = dynamodb.Table(table_name)
    
    response = update_item_with_retry(table, 'accession', item_name, f"SET {name} = :val, {name2} = :val2, {name3} = :val3, {name4} = :val4, {name5} = :val5", {
                ':val': str(value),
                ':val2': str(value2),
                ':val3': str(value3),
                ':val4': str(value4),
                ':val5': str(value5),
                })
    return response

"""
 atomic update of counters inside dynamodb
"""
def ddb_counter(
        current_date,
        counter_name, val,
        region='us-east-1'
    ):
    session = boto3.Session(region_name=region)
    dynamodb = session.resource('dynamodb')
  
    table = dynamodb.Table("Logan-Counters")
    
    current_date += " #" + str(randint(0, 99))
   
    response = update_item_with_retry(table, 'date', current_date, f"SET {counter_name} = if_not_exists({counter_name}, :start) + :val", {
                   ':val': decimal.Decimal(val),
                   ':start': decimal.Decimal(0)
                })
    return response

def ddb_counter2(
        current_date,
        counter_name, val,
        counter_name2, val2,
        region='us-east-1'
    ):
    session = boto3.Session(region_name=region)
    dynamodb = session.resource('dynamodb')
  
    table = dynamodb.Table("Logan-Counters")

    current_date += " #" + str(randint(0, 99))
   
    response = update_item_with_retry(table, 'date', current_date, f"SET {counter_name} = if_not_exists({counter_name}, :start) + :val, {counter_name2} = if_not_exists({counter_name2}, :start) + :val2", {
                   ':val': decimal.Decimal(val),
                   ':val2': decimal.Decimal(val2),
                   ':start': decimal.Decimal(0)
                })
    return response


def stream_output(pipe, target_list, is_stderr=False, shared_state=None, max_wait_lock=None):
    print_stderr = False 
    warning_printed = False

    for line in iter(pipe.readline, ''):

        with max_wait_lock:
            current_time = time.time()
            if shared_state['last_output_time'] == 0:
                shared_state['last_output_time'] = current_time
            else:
                wait_time = current_time - shared_state['last_output_time']
                shared_state['last_output_time'] = current_time
                if wait_time > shared_state['max_wait_time']:
                    shared_state['max_wait_time'] = wait_time

        if sys.getsizeof(target_list) > 1000000:
            if not warning_printed:
                warning_printed = True
                print(f"{'stderr' if is_stderr else 'stdout'} output getting way too big, not logging it any more. Last 5 lines:", target_list[-5:], flush=True)
        else:
            target_list.append(line)

        if is_stderr:
            if print_stderr:
                print(f"stderr: {line.strip()}", flush=True)
        else:
            print(line.strip(), flush=True)
    pipe.close()

#variable to store the maximum disk usage
max_disk_usage = 0
disk_monitor_stop = None

def get_disk_usage(path="."):
    """Return disk usage of the specified path."""
    # no, not that way, will be confused by multiple jobs
    #usage = shutil.disk_usage(path)
    #return int(usage.used / (1024 * 1024)) # in MB
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(path):
        for file in filenames:
            file_path = os.path.join(dirpath, file)
            try:
                total_size += os.path.getsize(file_path)
            except:
                pass
    return int(total_size / (1024 * 1024)) # in MB

def monitor_disk_usage(interval=5, path="."):
    """Monitor disk usage at specified interval and store the maximum usage."""
    global max_disk_usage, disk_monitor_stop
    while not disk_monitor_stop:
        usage = get_disk_usage(path)
        max_disk_usage = max(max_disk_usage, usage)
        time.sleep(interval)

def monitor_output_timeout(shared_state, stop_monitoring, max_wait_lock, proc, timeout=60):
    """
    Monitors the time since the last output. If it exceeds the timeout,
    terminates the process. Stops monitoring when signaled by the main thread.
    """
    import psutil
    def kill(proc_pid):
        process = psutil.Process(proc_pid)
        for proc in process.children(recursive=True):
            proc.kill()
        process.kill()
    warning_printed = False
    warning_printed2 = False
    while not stop_monitoring.is_set():
        with max_wait_lock:
            if shared_state['last_output_time'] > 0:
                time_since_last_output = time.time() - shared_state['last_output_time']
                if time_since_last_output > timeout*0.8 and (not warning_printed):
                    print("Warning, will kill the process soon due to timeout.", flush=True)
                    import sys
                    sys.stdout.flush()
                    warning_printed = True
                if time_since_last_output > timeout and not (warning_printed2):
                    print("Warning, would have killed the process due to timeout! Giving it a little bit more time", flush=True)
                    warning_printed2 = True
                if time_since_last_output > 1.1*timeout:
                    print(f"No output for {timeout} seconds. Terminating process.", flush=True)
                    kill(proc.pid)
                    break
        time.sleep(1)

"""
returns time in seconds
memory in MB
"""
def get_memory_and_time(command, timeout=0):
    global disk_monitor_stop, max_disk_usage

    # Construct the full command with /usr/bin/time to get memory usage and other stats
    full_command = ["/usr/bin/time", "-v"] + command
    print("Executing command: "," ".join(full_command), flush=True)
    
    start_time = time.time()
    
    # Using Popen to get real-time output

    #os.environ["LD_PRELOAD"] = "/usr/lib/libmimalloc.so"
    #proc = subprocess.Popen(full_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, env=os.environ)
    proc = subprocess.Popen(full_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    stdout_data = []
    stderr_data = []

    # Shared variables for maximum wait times
    # This captures both stderr and stdout
    shared_state = {
      'max_wait_time': 0,
      'last_output_time': 0,
    }

    # Locks for thread-safe updates
    max_wait_lock = threading.Lock()

    stdout_thread = threading.Thread(target=stream_output, args=(proc.stdout, stdout_data, False, shared_state, max_wait_lock))
    stderr_thread = threading.Thread(target=stream_output, args=(proc.stderr, stderr_data, True, shared_state, max_wait_lock))

    stdout_thread.start()
    stderr_thread.start()

    disk_monitor_thread = threading.Thread(target=monitor_disk_usage)
    disk_monitor_thread.start()
    disk_monitor_stop = False

    # Monitoring thread that kills process if it hasn't output anything in a while
    if timeout > 0:
        stop_monitoring = threading.Event()
        timeout_thread = threading.Thread(target=monitor_output_timeout, args=(shared_state, stop_monitoring, max_wait_lock, proc, timeout))
        timeout_thread.start()

    stdout_thread.join()
    stderr_thread.join()

    disk_monitor_stop = True
    #disk_monitor_thread.join() #no need to wait

    if timeout > 0:
        # Once the subprocess is done, signal the monitoring thread to stop
        stop_monitoring.set()
        timeout_thread.join()
    
    end_time = time.time()

    retcode = proc.wait()
    if retcode:
        error = subprocess.CalledProcessError(proc.returncode, full_command)
        error.stdout = ''.join(stdout_data)
        error.stderr = ''.join(stderr_data)
        raise error

    elapsed_time = end_time - start_time

    max_wait = shared_state['max_wait_time']
    
    print(f"Command succeeded in {elapsed_time:.1f} seconds with error code {retcode}.", flush=True)
    print(f"Max wait between outputs: {max_wait:.1f} seconds", flush=True)
    #print(f"Last 15 lines stderr:", flush=True) # not interesting because /usr/bin/time
    #print(''.join(stderr_data[-15:]), flush=True)

    memory = 0
    percent_cpu = 0
    for line in stderr_data:
        if 'Maximum resident set size' in line:
            memory = int(line.split()[-1])  # Grab the last value which is the memory in Kbytes
            memory = memory / 1024 # Convert to megabytes
        if 'Percent of CPU this job got' in line:
            percent_cpu = line.split()[-1][:-1]
    
    print(f"Max RSS: {memory:.1f} MB", flush=True)
    print(f"CPU: {percent_cpu}%", flush=True)
    print(f"Max disk: {max_disk_usage:.1f} MB", flush=True)
    
    return elapsed_time, memory, int(percent_cpu), max_disk_usage, max_wait, ''.join(stderr_data), ''.join(stdout_data)

import platform

def get_machine_architecture():
    architecture = platform.machine()
    
    if architecture == "x86_64":
        return "x64"
    elif architecture in ["aarch64", "ARM64"]:
        return "ARM64"
    else:
        return "Unknown"

def sra_get_size(filename):
    start_time = time.time()
    cmd = f"sra-stat --quick -x {filename} | xmllint --xpath '//Table[@name=\"SEQUENCE\"]/Statistics/Elements/@count' - | cut -d'=' -f2 | tr -d '\"'"
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    if process.returncode != 0:
        raise Exception(f"Error executing sra_get_size command: {error.decode('utf-8')}")
    nb_bases = output.decode('utf-8').strip()
    end_time = time.time()
    print(f"sra-stat executed in {end_time-start_time:.1f} seconds (ret={process.returncode}).", flush=True)
    print(f"Found number of bases: {nb_bases}", flush=True)
    if not nb_bases.isdigit():
        print(f"sra_get_size first method didn't work ({nb_bases}), trying alternative method", flush=True)
        cmd = f"sra-stat --quick -x {filename} | xmllint --xpath 'string(/Run/@base_count)' -"
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        output, error = process.communicate()
        nb_bases = output.decode('utf-8').strip()
        if process.returncode != 0:
            raise Exception(f"Error executing sra_get_size second command: {error.decode('utf-8')}")
        if not nb_bases.isdigit():
            print(f"sra_get_size second method didn't work ({nb_bases}), giving up", flush=True)
            nb_bases = "0"
    return int(nb_bases)
