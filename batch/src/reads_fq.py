import os
import utils
from utils import ddb_log, get_memory_and_time
from datetime import datetime
import glob
from constants import *
import subprocess

def get_reads(accession, s3, s3_folder, outputBucket, nb_threads, inputDataFn):
    local_file = accession + ".fasta"

    print("downloading reads from SRA to EBS..",flush=True)
    startTime = datetime.now()
    
    os.system('vdb-config --prefetch-to-cwd')
    os.system('vdb-config --report-cloud-identity yes')
    os.system('vdb-config --simplified-quality-scores yes')

    ret = 0
    max_disk = 0
    
    prefetch_start = datetime.now()
    pret = os.system('prefetch --max-size 200G ' + accession)
    if pret > 0:
        ret |= PREFETCH_FAILED
    prefetch_time = datetime.now() - prefetch_start
    if ret == 0:
        ddb_log(accession,'prefetch_time',int(prefetch_time.seconds))

    pfqdump_start = datetime.now()
    try:
        runtime, mem, percent_cpu, max_disk, max_wait, stderr, stdout = get_memory_and_time(['fasterq-dump','--fasta-unsorted',accession])
        print(f"fasterq-dump succeeded")
    except subprocess.CalledProcessError as e:
        ret |= FASTERQ_DUMP_FAILED
        return ret, 0

    import re
    if re.search(r'reads written\s+:\s+0', stderr):
        print("Ah, but there are no reads.")
        # hack to upload an empty unitigs file
        output_filename = accession+".unitigs"
        os.system("touch "+output_filename+".fa")
        #os.system(' '.join(["lz4","-z",output_filename+".fa", output_filename+".fa.lz4"]))
        #output_filename += ".fa.lz4"
        output_filename += ".fa"
        s3.upload_file(output_filename, outputBucket, s3_folder + output_filename, ExtraArgs={'ACL': 'public-read'})
   
    pfqdump_time = datetime.now() - pfqdump_start
    if ret == 0:
        ddb_log(accession,'fqdump_time',int(pfqdump_time.seconds))

    files = glob.glob(os.getcwd() + "/" + accession + "*")
    print("after fasterq-dump, dir listing: ", files,flush=True)

    # cleanup
    os.system(' '.join(["rm","-Rf",accession]))

    # check if fasta was written (maybe it wasn't, because there of no reads)
    if not os.path.exists(local_file):
        print("no reads written, stopping")
        return ret, 0

    file_size = str(os.stat(local_file).st_size)
    ddb_log(accession,'file_size',file_size)

    endTime = datetime.now()
    diffTime = endTime - startTime
    if ret == 0:
        ddb_log(accession,'erc_batch_assembly_dl_time',int(diffTime.seconds))
        ddb_log(accession,'erc_batch_assembly_dl_date',str(datetime.now()))
    print(accession, "Reads processing time - " + str(diffTime.seconds),flush=True) 

    return ret, max_disk
