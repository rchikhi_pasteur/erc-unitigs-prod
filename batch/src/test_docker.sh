#!/bin/bash
set -e

# to run it on different cores
cpuset_index=$(if [ -n "$1" ]; then echo $1; else echo 0; fi)

aws sts get-session-token --duration-seconds 6000 > credentials.json

echo "AWS_ACCESS_KEY_ID=$(jq -r '.Credentials.AccessKeyId' credentials.json)" > credentials.env
echo "AWS_SECRET_ACCESS_KEY=$(jq -r '.Credentials.SecretAccessKey' credentials.json)" >> credentials.env 
echo "AWS_SESSION_TOKEN=$(jq -r '.Credentials.SessionToken' credentials.json)" >> credentials.env 

NAME=batch-unitigs-test-local-job
#NOCACHE=--no-cache
docker build $NOCACHE -t $NAME \
             --build-arg AWS_DEFAULT_REGION=us-east-1 \
              .

# SRR11366719 is a small file
# SRR9156994 is 1 Gbases
# SRR7696661 is 27 Gbases yeast
# SRR11905265 is 33 Gbases vicugna
# SRR21012863 has no solid 32-mers
# SRR709914 has no reads (fasterq-dump fails)
# SRR10912254 has unitigs but no contigs
# DRR075766 is one that repeatedly OOMs
# ERR589596 another one that OOMs
# DRR082807 is one where sra-stat --quick has different format
# SRR10025398 huge unitigs output
# ERR976749 has aligned reads

#MODE="heavy"
#MODE="light"
MODE="tiny"
# Check if a special mode is activated
if [[ "${MODE}" == "heavy" ]]; then
	accession=ERR485981
	threads=16
	memorylimit="--memory=16g"
elif [[ "${MODE}" == "light" ]]; then
	accession=SRR6279421
	threads=4
	memorylimit="--memory=7g"
elif [[ "${MODE}" == "tiny" ]]; then
	accession=SRR13859363
	threads=4
	memorylimit="--memory=7g"
fi
cpuset_start="$(($cpuset_index))"
cpuset_end="$(($cpuset_index+threads-1))"

bucket=$(if [[ -z $(aws sts get-caller-identity |grep serratus-rayan) ]]; then echo "logan-dec2023-testbucket"; else echo "logan-testing-march2024"; fi)
echo "output bucket: $bucket"

docker run \
    $memorylimit \
    --cpuset-cpus $cpuset_start-$cpuset_end\
    --ulimit nofile=65535:65535 \
    --env-file credentials.env \
    -e AWS_DEFAULT_REGION=us-east-1\
    -e Accessions=$accession\
    -e Threads=$threads\
    -e OutputBucket=$bucket\
    -e ForceBuildUnitigs=true\
    $NAME \
    python simple_batch_processor.py 

rm -f credentials.json credentials.env
