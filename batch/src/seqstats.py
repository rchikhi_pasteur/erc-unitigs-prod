from datetime import datetime
from utils import ddb_log, ddb_log4, get_memory_and_time, get_machine_architecture
import os
import re
import subprocess
import constants

def parse_seqkit_output(output):
    lines = output.split('\n')  # Split the output into lines
    headers = lines[0].split()  # Split the first line to get the headers
    
    data_dict = {}
    if len(lines) > 1:
        values = lines[1].split()  # Split the second line to get the values
        for i, header in enumerate(headers):
            # Removing commas to handle numeric values properly
            value = values[i].replace(',', '')  
            if value.replace('.', '').isnumeric():  # Check if the value is numeric
                if '.' in value:  # Check if the value is a floating number
                    data_dict[header] = float(value)  # Convert the value to float
                else:
                    data_dict[header] = int(value)  # Convert the value to int
            else:
                data_dict[header] = value  # Keep the value as string
    
    return data_dict

def seqstats(accession, what="unitigs"):
    start_time = datetime.now()
    arch = get_machine_architecture()
    ret = constants.SEQSTATS_FAILED
    tigs_file = accession+f".{what}.fa"
    if not os.path.exists(tigs_file) or \
            os.path.getsize(tigs_file) == 0:
        return 0, 0
    nbseq=0
    try:
        runtime, mem, percent_cpu, max_disk, max_wait, stderr, stdout = get_memory_and_time(
                        ["/seqkit","stats","-T","-a",tigs_file])
        d = parse_seqkit_output(stdout)
        n50 = d['N50']
        nbseq = d['num_seqs']
        sumlen = d['sum_len']
        maxlen = d['max_len']
        ddb_log4(accession,f'seqstats_{what}_n50',n50,
                           f'seqstats_{what}_nbseq',nbseq,
                           f'seqstats_{what}_sumlen',sumlen,
                           f'seqstats_{what}_maxlen',maxlen)
        ret = 0
    except subprocess.CalledProcessError as e:
        print(f"Seqstats failed.")
        print(f"Command: {e.cmd}")
        print(f"Return Code: {e.returncode}")
        print("----")
        print(f"Stdout: {e.stdout}")
        print("----")
        print(f"Stderr: {e.stderr}")
        print("----")

    seqstats_time = datetime.now() - start_time
    print("Seqstats time:",seqstats_time)
    return ret, nbseq


