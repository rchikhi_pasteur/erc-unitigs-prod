from datetime import datetime
from utils import ddb_log, ddb_log4, get_memory_and_time, get_machine_architecture
import subprocess
import os
import re
import constants
import boto3

def unitigs(accession, local_file, outputBucket, nb_threads):
    start_time = datetime.now()
    #program="ggcat"
    program="cuttlefish"
    #program="bcalm2"
    arch = get_machine_architecture()
    k_value = str(31)
    ret = constants.UNITIGS_FAILED
    max_disk = 0
    #os.system("ulimit -n 49152") #should be enough for anybody, as 192 threads divided by 8 is 24, 2048*24=49152
    os.system("ulimit -n 2048") 

    if program == "ggcat":
        output_filename = accession+".unitigs.fa.lz4"
        try:
            runtime, mem, percent_cpu, max_disk, max_wait, stderr, stdout = get_memory_and_time(["/usr/bin/time", 
                            "/root/.cargo/bin/ggcat", 
                            "build", 
                            local_file,
                            "-k",k_value,
                            "-j",nb_threads,"-o",output_filename])
            ddb_log(accession,'ggcat_'+arch+'_mem',mem)
            ddb_log(accession,'ggcat_'+arch+'_time',runtime)
            ddb_log(accession,'ggcat_'+arch+'_percent_cpu',percent_cpu)
            ret = 0
        except subprocess.CalledProcessError:
            print(f"Ggcat failed.")
    elif program == "cuttlefish":
        output_filename = accession+".unitigs"
        max_attempts = 1
        for attempt in range(1, max_attempts + 1):
            try:
                # timeout policy for cuttlefish
                def calculate_timeout(dataset_size_mbases):
                    SMALL_DATASET_THRESHOLD = 1000  # MBases
                    SMALL_DATASET_TIMEOUT = 400  # seconds
                    LARGE_DATASET_RATIO = 0.25
                    if dataset_size_mbases <= SMALL_DATASET_THRESHOLD:
                        timeout = SMALL_DATASET_TIMEOUT
                    else:
                        if dataset_size_mbases <= 10000:
                            timeout = dataset_size_mbases * LARGE_DATASET_RATIO
                        else:
                            # small tweak for larger datasets above 10 GB
                            timeout = dataset_size_mbases * 0.2
    
                    return timeout
                dataset_size_mbases = os.stat(local_file).st_size / (1024*1024)
                timeout = calculate_timeout(dataset_size_mbases)
                print(f"Calculated no-output timeout: {timeout:.1f} seconds")

                runtime, mem, percent_cpu, max_disk, max_wait, stderr, stdout = get_memory_and_time(["/cuttlefish/bin/cuttlefish", 
                                   "build",
                                   "-m",str((5*int(nb_threads))//4),
                                   "-s",local_file,"-k",k_value,"-t",nb_threads,"--read",
                                   "-o",output_filename],
                                    timeout)

                # check that faster-dump didn't segfault
                seg_fault_pattern = r"fatal:\s*SIGNAL\s*-\s*Segmentation\s*fault"
                if re.search(seg_fault_pattern, stdout) or re.search(seg_fault_pattern, stderr):
                    print("Error: Segmentation fault detected in fasterq-dump.")
                    break
                if re.search("err:", stdout) or re.search("err:", stderr):
                    print("Error: Error detected in fasterq-dump.")
                    break

                ddb_log4(accession,'cuttlefisha_'+arch+'_mem',mem,
                         'cuttlefisha_'+arch+'_time',runtime,
                         'cuttlefisha_'+arch+'_percent_cpu',percent_cpu,
                         'cuttlefisha_'+arch+'_max_wait',max_wait)
                print(f"Cuttlefish succeeded on attempt {attempt}.")
                ret = 0
                break
            except subprocess.CalledProcessError as e:
                print(f"Cuttlefish failed on attempt {attempt}.")
                # just make sure it's not one of those empty datasets
                if re.search(r'Number\s+of\s+solid\s+32-mers:\s+0\.', e.stdout):
                    print("Ah, but the dataset has 0 k-mers.")
                    os.system("touch "+output_filename+".fa") #useful for my later scripts that check if the unitigs were created or not
                    ret = 0
                    break
                print(f"Command: {e.cmd}")
                print(f"Return Code: {e.returncode}")
                print("----")
                print(f"Stdout: {e.stdout}")
                print("----")
                print(f"Stderr: {e.stderr}")
                print("----")
                if attempt < max_attempts: print("Retrying...")
        #os.system(' '.join(["lz4","-z",output_filename+".fa", output_filename+".fa.lz4"]))
    elif program == "bcalm2":
        output_filename = accession+".unitigs"
        try:
            runtime, mem, percent_cpu, max_disk, max_wait, stderr, stdout = get_memory_and_time(["/bcalm/build/bcalm", 
                                "-in",local_file,"-kmer-size",k_value,"-nb-cores",nb_threads,
                                "-out",accession])
            ddb_log(accession,'bcalm2_'+arch+'_mem',mem)
            ddb_log(accession,'bcalm2_'+arch+'_time',runtime)
            ddb_log(accession,'bcalm2_'+arch+'_percent_cpu',percent_cpu)
            print(f"Bcalm succeeded.")
            ret = 0
        except subprocess.CalledProcessError:
            print(f"Bcalm failed.")
        #os.system(' '.join(["lz4","-z",output_filename+".fa", output_filename+".fa.lz4"]))

    else:
        exit("unknown program: " + binary_name)

    unitigs_time = datetime.now() - start_time

    return ret, max_disk


