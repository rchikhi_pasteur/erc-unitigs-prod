# constructs unitigs and and uploads them
# derivative of serratus-batch-assembly

import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError
import csv, sys, argparse
from datetime import datetime
import json
import os
import urllib3
import glob
import shutil

import constants
import subprocess

import reads_sra
import unitigs 
import seqstats
import contigs
from utils import ddb_log, ddb_log3, ddb_log4, ddb_log5, ddb_counter, ddb_counter2, get_memory_and_time, get_machine_architecture, s3_upload_file, sra_get_size, s3_make_unitigs_path ,s3_make_contigs_path

LOGTYPE_ERROR = 'ERROR'
LOGTYPE_INFO = 'INFO'
LOGTYPE_DEBUG = 'DEBUG'


def make_unitigs(accession, outputBucket, s3, nb_threads, current_date):
    reads_retval, max_disk = reads_sra.get_reads(accession, s3, outputBucket)
    arch = get_machine_architecture()
    retval = reads_retval

    if retval > 0:
        ddb_log(accession,arch+'_return_value',retval)
        return retval, 0, 0, 0
    
    # check if sra was correctly retrieved
    local_file = accession + ".sra"
    if os.path.exists(local_file):
        try:
            input_size = sra_get_size(local_file)
        except Exception as e:
            print(f"Failed to execute sra-stat: {e}",flush=True)
            return constants.PREFETCH_FAILED, 0, 0, 0
        if input_size == 0:
            ddb_counter(current_date,"nb_accessions_no_size",1)
        ddb_counter2(current_date,"nb_accessions_reads_downloaded",1,
                                  "input_bases_attempted",input_size)
        unitigs_retval, unitigs_max_disk = unitigs.unitigs(accession, local_file, outputBucket, nb_threads)
        retval |= unitigs_retval
        max_disk = max(max_disk, unitigs_max_disk)
    else:
        return 0,0,0,0 #will be a graceful exit in main script (404 for SRA file or for one of the remote references)

    if retval > 0:
        ddb_log(accession,arch+'_return_value',retval)
        return retval, 0, 0, 0

    # compute n50 etc
    seqstats_retval, nbseq = seqstats.seqstats(accession, what="unitigs")
    retval |= seqstats_retval
    
    return retval, max_disk, input_size, nbseq

def are_unitigs_already_done(accession, outputBucket, s3):
    try:
        key = s3_make_unitigs_path(accession + ".unitigs.fa.zst")
        s3.head_object(Bucket=outputBucket, Key=key)
        return True, 0
    except ClientError as e:
        if e.response['Error']['Code'] == '404':
            return False, 0
        else:
            print("Error checking if unitigs are present: " + str(e),flush=True)
            return False, constants.UNKNOWN_ERROR  

def are_contigs_already_done(accession, outputBucket, s3):
    try:
        key = s3_make_contigs_path(accession + ".contigs.fa.zst")
        s3.head_object(Bucket=outputBucket, Key=key)
        return True, 0
    except ClientError as e:
        if e.response['Error']['Code'] == '404':
            return False, 0
        else:
            print("Error checking if contigs are present: " + str(e),flush=True)
            return False, constants.UNKNOWN_ERROR  


def grab_unitigs(accession, outputBucket, s3):
    unitigs_filename_zstd = accession + ".unitigs.fa.zst"
    unitigs_filename = accession + ".unitigs.fa"
    try:
        key = s3_make_unitigs_path(unitigs_filename_zstd)
        s3.download_file(Bucket=outputBucket, Key=key, Filename=unitigs_filename_zstd)
        runtime, mem, percent_cpu, max_disk, max_wait, stderr, stdout = get_memory_and_time([
                        "zstd",
                        "-d",
                        unitigs_filename_zstd])
        get_memory_and_time([
             "perl","-i","-pe","if (/^>/) { s/^>"+accession+"_/>/; }",
            unitigs_filename])
        print("Got unitigs from S3, file size:",str(os.stat(unitigs_filename).st_size),flush=True)
        return 0
    except:
        print("Error getting unitigs from S3",flush=True)
        return constants.UNKNOWN_ERROR  

def compress_and_upload_tigs(what, accession, outputBucket, nb_threads, level=12):
    try: 
        tigs_filename = accession + "." + what + ".fa"
        if not os.path.exists(tigs_filename):
            print("Cannot find " + what,flush=True)
            return constants.UNKNOWN_ERROR
        size_before = os.stat(tigs_filename).st_size
        runtime, mem, percent_cpu, max_disk, max_wait, stderr, stdout = get_memory_and_time([
                        "f2sz",
                        "-l", str(level), # compression level 12
                        "-b", "128M", # block size 128M
                        "-F", # aligns frames to FASTA format
                        "-i", # create in-file frame index
                        "-f", # overwrite output
                        "-T", nb_threads,
                        tigs_filename])
        tigs_filename = accession + "." + what + ".fa.zst"
        size_after = os.stat(tigs_filename).st_size
        ddb_log(accession, "size_" + what + "_before_compression", size_before)
        ddb_log(accession,"size_" + what + "_after_compression", size_after)
        if os.path.exists(tigs_filename):
            if what == "contigs":
                path = s3_make_contigs_path(tigs_filename)
            elif what == "unitigs":
                path = s3_make_unitigs_path(tigs_filename)
            else:
                print("error: unknown type to upload "+what,flush=True)
                exit(1)
            s3_upload_file(tigs_filename, outputBucket, path)
        else:
            print(f"Error, {tigs_filename} does not exist after f2sz compression, can't upload it",flush=True)
            return constants.UNKNOWN_ERROR
    except:
        print("Error compressing and uploading " + what,flush=True)
        import traceback
        traceback.print_exc()
        sys.stdout.flush()
        return constants.UNKNOWN_ERROR  
    return 0


def process_file(accession, region, nb_threads, outputBucket, force_build_unitigs, only_unitigs):
    current_utc = datetime.utcnow()
    current_date = current_utc.strftime("%Y-%m-%d %Hh UTC")
    
    urllib3.disable_warnings()
    arch = get_machine_architecture()
    s3 = boto3.client('s3')
    ddb_counter(current_date, "nb_accessions_attempted",1)
    retval = 0
    input_size = 0 # sometimes 0 as we're not always dealing with reads, if unitigs were already made

    print("docker image - prod", flush=True)
    print("date - " + current_date, flush=True)
    print("region - " + region, flush=True)
    print("accession - " + accession, flush=True)
    print("threads - " + nb_threads, flush=True)
    if only_unitigs: print("only unitigs",flush=True)
    if force_build_unitigs: print("force building unitigs",flush=True)

    try:
        instance_type = subprocess.check_output(
            ['curl', '-s', 'http://169.254.169.254/latest/meta-data/instance-type']
            ).decode('utf-8').strip()
        print("instance type - " + instance_type,flush=True)
    except Exception as e:
        print("Failed to retrieve instance type: {e}",flush=True)
        retval = constants.UNKNOWN_ERROR
        return retval
 
    try:
        instance_id = subprocess.check_output(
            ['curl', '-s', 'http://169.254.169.254/latest/meta-data/instance-id']
            ).decode('utf-8').strip()
        print("instance ID - " + instance_id,flush=True)
        job_id = str(os.environ.get('AWS_BATCH_JOB_ID'))
        print("Job ID - " + job_id)
    except Exception as e:
        print(f"Failed to retrieve instance ID: {e}",flush=True)
        retval = constants.UNKNOWN_ERROR
        return retval
    
    # check free space
    os.chdir("/")
    print("free space on /", flush=True)
    os.system(' '.join(["df", "-h", "."]))
    
    # check that serratus-data is big enough (no EBS problem)
    total, used, free = shutil.disk_usage("/serratus-data/")
    serratus_data_in_gb = total // (2**30)
    if serratus_data_in_gb < 40:
        print("/serratus-data/ size",serratus_data_in_gb,"GB",flush=True)
        print("/serratus-data/ too small! probably EBS problem, bad instance, stopping",flush=True)
        exit(1)

    if os.path.exists("/serratus-data/"+accession):
        print(f"huh? path /serratus-data/{accession} already exists?!",flush=True)
        print("How's that possible, unless you ran the same job at the same time, same machine?",flush=True)
        retval = constants.UNKNOWN_ERROR
        return retval

    # necessary because cuttlefish uses the same temp files names!
    os.mkdir("/serratus-data/"+accession)
    os.chdir("/serratus-data/"+accession)
    print("free space on /serratus-data/"+accession, flush=True)
    os.system(' '.join(["df", "-h", "."]))

    startBatchTime = datetime.now()

    contigs_already_exist, retval = are_contigs_already_done(accession, outputBucket, s3)

    if retval > 0:
        ddb_log(accession,arch+'_return_value',retval)
        return retval

    if contigs_already_exist and (not force_build_unitigs):
        print(f"Contigs already exist for accession {accession} in s3://{outputBucket}/, nothing to do here.",flush=True)
        return 0

    unitigs_already_exist, retval = are_unitigs_already_done(accession, outputBucket, s3)

    if retval > 0:
        ddb_log(accession,arch+'_return_value',retval)
        return retval

    if unitigs_already_exist and (not force_build_unitigs):
        print(f"Unitigs file already exists on S3 for accession {accession} in s3://{outputBucket}/, downloading it..",flush=True)
        retval = grab_unitigs(accession, outputBucket, s3)
        max_disk = 0
    else:
        retval, max_disk, input_size, nbseq = make_unitigs(accession, outputBucket, s3, nb_threads, current_date)
        local_file = accession + ".sra"
        if not os.path.exists(local_file):
            print(f"{accession}.sra file not present, exiting gracefully.",flush=True)
            return 0 

        # if more than 500,000,000 unitigs for 16 threads, so, about 30M unitigs per thread, exit because minia has no chance
        if int(nbseq) > 30_000_000 * int(nb_threads):
            print(f"Too many unitigs to feed to minia with {int(nb_threads)*2} GB memory, switching to only_unitigs=True",flush=True)
            only_unitigs = True

        if retval > 0:
            ddb_log(accession,arch+'_return_value',retval)
            return retval
        if not only_unitigs:
            retval = compress_and_upload_tigs("unitigs", accession, outputBucket, nb_threads, level=1)
    
    if retval > 0:
        ddb_log(accession,arch+'_return_value',retval)
        return retval
    ddb_counter(current_date,"nb_accessions_unitigs_made",1)
  
    if not only_unitigs:
        # make contigs
        contigs_retval, contigs_max_disk = contigs.contigs(accession, outputBucket, nb_threads)
        retval |= contigs_retval
        max_disk = max(max_disk, contigs_max_disk)
        
        # compute n50 of contigs
        seqstats_retval, nbseq = seqstats.seqstats(accession, what="contigs")
        retval |= seqstats_retval
    else:
        contigs_retval = 0

    # re-upload unitigs, this time with links, if minia succeeded. also upload contigs
    if contigs_retval == 0:
        tasks = ["unitigs"]
        if not only_unitigs:
            tasks += ["contigs"]
        for what in tasks:
            # also rename headers so that they're pleasant
            tigs_filename = accession + "." + what + ".fa"
            # fix some headers
            if what == "contigs":
                get_memory_and_time([
                    "perl","-i","-pe","if (/^>/) { s/^>/>"+accession+"_/; s/km:/ka:/; s/LN:i:\\S+ //; s/KC:i:\\S+ //; }",
                    tigs_filename])
            if what == "unitigs":
                get_memory_and_time([
                    "perl","-i","-pe","if (/^>/) { s/^>/>"+accession+"_/; s/km:/ka:/; }",
                    tigs_filename ])
            if os.stat(tigs_filename).st_size > 0:
                compress_and_upload_tigs(what, accession, outputBucket, nb_threads)
    # finishing up
    endBatchTime = datetime.now()
    diffTime = endBatchTime - startBatchTime
    print(accession, "Complete pipeline processing time: " + str(diffTime.seconds) + " seconds", flush=True) 

    ddb_log(accession,arch+'_return_value',retval)
    if retval == 0:
        ddb_log5(accession,arch+'_processing_time',diffTime.seconds,
                          arch+'_processing_date',str(datetime.now()),
                          arch+'_instance_type',instance_type,
                          arch+'_instance_id',instance_id,
                          arch+'_job_id',job_id)
        if not unitigs_already_exist: # don't record if we're doing just the contigs, minia needs almost no disk
                                      # (except for creating unitigs links)
            ddb_log(accession,arch+'_max_disk',max_disk)
        ddb_counter2(current_date,"nb_accessions_succeeded",1,
                                  "input_bases_succeeded",input_size)

    return retval

def process_accession(accession, region, nb_threads, output_bucket, force_build_unitigs, only_unitigs):
    retval = 0
    try:
        retval |= process_file(accession, region, nb_threads, output_bucket, force_build_unitigs, only_unitigs)
    except Exception as ex:
        print("Exception occurred during process_file() with arguments", accession, region,flush=True) 
        print(ex,flush=True)
        import traceback
        traceback.print_exc()
        sys.stdout.flush()
        retval = constants.UNKNOWN_ERROR

    # it is important that this code isn't in process_file() as that function may stop for any reason
    print("Final retval:",retval,flush=True)
    print("checking free memory",flush=True)
    os.system(' '.join(["free","-m"])) 
    os.chdir("/serratus-data")
    if os.path.exists('/serratus-data/refseq'):
        print("/serratus-data/refseq disk usage",flush=True)
        os.system(' '.join(["du", "-hs", "refseq"]))
    print(f"contents of /serratus-data/{accession}",flush=True)
    os.system(' '.join(["ls","-Ral",accession+"*"])) 
    print("checking free space on /serratus-data",flush=True)
    os.system(' '.join(["df", "-h", "."]))
    os.system(' '.join(["df", "-h", "/"]))

    #cleanup
    os.system(' '.join(["rm","-Rf","/serratus-data/"+accession]))
    return retval


def main():
    accessions = ""
    region = "us-east-1"
    nb_threads = str(4)
    force_build_unitigs = False
    only_unitigs = False
    output_bucket = ""

    if "Accessions" in os.environ:
        accessions = os.environ.get("Accessions")
    if "Region" in os.environ:
        region = os.environ.get("Region")
    if "Threads" in os.environ:
        nb_threads = os.environ.get("Threads")
    if "ForceBuildUnitigs" in os.environ:
        force_build_unitigs = len(os.environ.get("ForceBuildUnitigs")) > 0
    if "OnlyUnitigs" in os.environ:
        only_unitigs = os.environ.get('OnlyUnitigs') == "True"
    if "OutputBucket" in os.environ:
        output_bucket = os.environ.get("OutputBucket")
    
    if len(accessions) == 0:
        exit("This script needs an environment variable Accessions set to something. It may be just one accession")
    
    if len(output_bucket) == 0:
        exit("This script needs an environment variable OutputBucket set to something")

    nb_acc_failed = 0
    nb_acc = 0
    for accession in accessions.split(','):
        retval = process_accession(accession, region, nb_threads, output_bucket, force_build_unitigs, only_unitigs)
        if retval > 0:
            nb_acc_failed += 1
        nb_acc += 1
    if nb_acc > 0:
        failrate = nb_acc_failed/nb_acc
        print(f"Pipeline success rate: {(1-failrate)*100:.1f}%",flush=True)
        # only fail the whole thing if >= 15% accessions are failing
        if failrate > 0.15:
            sys.exit(1)

if __name__ == '__main__':
   main()
