import boto3
import sys

def submit_array_job(num_jobs, tag, s3_path, job_definition, job_queue, threads, output_bucket, only_unitigs, force_build_unitigs):
    batch = boto3.client('batch')
    region = "us-east-1"
    
    response = batch.submit_job(
        jobName=tag,
        jobQueue=job_queue,
        arrayProperties={'size': num_jobs},
        jobDefinition=job_definition,
        tags={"IndexThePlanet": tag},
        propagateTags=True,
        timeout={
            'attemptDurationSeconds': int(threads)*3500 
        },
        containerOverrides={
            "command": ["python", "array_batch_processor.py"],
            "environment": [
                {"name": "S3_PATH", "value": s3_path},
                {"name": "Region", "value": region},
                {"name": "Threads", "value": threads},
                {"name": "OutputBucket", "value": output_bucket},
                {"name": "OnlyUnitigs", "value": only_unitigs},
                {"name": "ForceBuildUnitigs", "value": force_build_unitigs},
            ]
        }
    )

    print(f"Job ID is {response['jobId']}.")

if __name__ == '__main__':
    if len(sys.argv) < 8:
        exit("Usage: python array_submit_job.py <num_jobs> <tag> <s3_path> <job_definition> <job_queue> <num_threads> <output_bucket>")
    
    num_jobs       = int(sys.argv[1])
    tag            = sys.argv[2]
    s3_path        = sys.argv[3]
    job_definition = sys.argv[4]
    job_queue      = sys.argv[5]
    threads        = sys.argv[6]
    output_bucket  = sys.argv[7]
    only_unitigs = str(False) #str(True) # CHANGEME
    #print("Submitting array with only_unitigs=True")
    force_build_unitigs = str(False) #str(True) # CHANGEME
    #print("Submitting array with force_build_unitigs=True")

    submit_array_job(num_jobs, tag, s3_path, job_definition, job_queue, threads, output_bucket, only_unitigs, force_build_unitigs)
