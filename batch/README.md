# Construction of unitigs on AWS Batch

### Installation 

You need to do a few manual steps to setup your AWS account for this execution.

1. Manually import the VPC stack (`0.VPC-Large-Scale.yaml`) into CloudFormation. Name that stack "Logan-LargeScaleVPC". For real production, select all Availability Zones, 6 zones in us-east-1. Everything else default with S3 Endpoint to True and DynamoDB to true. It's not automated but you only need to do it once.

2. Create a DynamoDB table called 'Logan', partition key is "accession".

Once these are  created, execute the below commands to spin up the infrastructure cloudformation stack.

```
./spinup.sh
./deploy-docker.sh
```

If you ever recreate the stack (e.g. after `cleanup.sh`), you don't need to run `deploy-docker.sh` unless the Dockerfile or scripts in `src/` were modified.

### Running a batch

1. `python simple_submit_job.py [accession] [accession_size] [tag] [output_bucket_name]`
2. In AWS Console > Batch, monitor how the job runs.


### Updating the infrastructure

Make sure that no job is running. In some cases, when making changes to `template.yaml` you can type:

```
./spinup.sh --update
```

to update it. But for big infrastructure changes, I prefer to cleanup+spinup.


### Code Cleanup

In short:

```
./cleanup.sh
```

Which deletes the CloudFormation stack.

What it doesn't do (needs to be done manually):

AWS Console > ECR - serratus-batch-unitigs-job - delete the image(s) that are pushed to the repository

## Monitoring dashboards

* Dashboard for monitoring instance and runtimes: https://github.com/rchikhi/aws-batch-runtime-monitoring
This is a fork of https://github.com/aws-samples/aws-batch-runtime-monitoring with cloudtrail creation automated.

* Dashboard for monitoring spot interruptions: https://github.com/aws-samples/ec2-spot-interruption-dashboard

* For cost monitoring, it's recommended to enable split cost allocation: https://docs.aws.amazon.com/cur/latest/userguide/enabling-split-cost-allocation-data.html

## License

This library is licensed under the MIT-0 License. See the LICENSE file.

### History

Taken from Serratus assembly AWS batch workflow

