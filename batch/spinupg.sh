aws s3 cp templateg.yaml s3://cf-templates-3ub7cutq9tcc-us-east-1
url=$(aws s3 presign  s3://cf-templates-3ub7cutq9tcc-us-east-1/templateg.yaml)

if [ "$1" == "--update" ]; then
    echo "updating graviton stack.."
    aws cloudformation update-stack --stack-name Logan-BatchUnitigsGraviton --template-url $url  --capabilities CAPABILITY_NAMED_IAM
else
    aws cloudformation create-stack --stack-name Logan-BatchUnitigsGraviton --template-url $url --capabilities CAPABILITY_NAMED_IAM
fi


