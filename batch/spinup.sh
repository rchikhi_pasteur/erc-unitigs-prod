#!/bin/bash -e

cfbucket=$(if [[ -z $(aws sts get-caller-identity |grep serratus-rayan) ]]; then echo "cf-templates-3ub7cutq9tcc-us-east-1"; else echo "serratus-rayan"; fi)
template=$(if [[ -z $(aws sts get-caller-identity |grep serratus-rayan) ]]; then echo "template.yaml"; else echo "template-serratus-mar24.yaml"; fi)

aws s3 cp $template s3://$cfbucket/indextheplanet-cftemplates/template.yaml
url=$(aws s3 presign  s3://$cfbucket/indextheplanet-cftemplates/template.yaml)

# Check for --update argument
if [ "$1" == "--update" ]; then
    echo "updating stack.."
    aws cloudformation update-stack --stack-name Logan-BatchUnitigs --template-url $url --capabilities CAPABILITY_NAMED_IAM
else
    aws cloudformation create-stack --stack-name Logan-BatchUnitigs --template-url $url --capabilities CAPABILITY_NAMED_IAM
fi

