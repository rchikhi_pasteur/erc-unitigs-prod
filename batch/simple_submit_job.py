import json
import boto3
import sys
import platform

architecture = platform.machine()

if len(sys.argv) < 5:
    exit("argument: [accessions] [accession_size] [tag] [output_bucket] [jobQueueIndex=0]")

jobQueueIndex=0

accessions = sys.argv[1]
accession_size = sys.argv[2]
tag = sys.argv[3]
output_bucket = sys.argv[4]
if len(sys.argv) > 5:
    jobQueueIndex= sys.argv[5]

if "RR" not in accessions:
    exit("accessions should be of the form: (E/S)RRxxxx,(E/S)RRxxxx,... A single accession is fine too.")
if not accession_size.isnumeric():
    exit("accession size should be a number")
if tag.isnumeric():
    exit("tag shouldn't be just a number")

batch = boto3.client('batch')
#region = batch.meta.region_name
region = "us-east-1"
jobQueue=f'IndexThePlanetJobQueueGraviton{jobQueueIndex}' if architecture == 'aarch64' else f'IndexThePlanetJobQueue{jobQueueIndex}'
#jobQueue="IndexThePlanetJobQueueDisques"

# Set jobDefinition and threads based on accession_size and architecture
if int(accession_size) < 20000:
    jobDefinition = 'logan-8gb-jobg' if architecture == 'aarch64' else 'indextheplanet-8gb-job'
    threads = str(4)
elif int(accession_size) < 60000:
    jobDefinition = 'logan-15gb-jobg' if architecture == 'aarch64' else 'indextheplanet-15gb-job'
    threads = str(8)
elif int(accession_size) < 180000:
    jobDefinition = 'logan-31gb-jobg' if architecture == 'aarch64' else 'indextheplanet-31gb-job'
    threads = str(16)
else:
    jobDefinition = 'logan-63gb-jobg' if architecture == 'aarch64' else 'indextheplanet-63gb-job'
    threads = str(32)

print("Submitting single job to Job Queue:",jobQueue)

jobname = accessions if "," not in accessions else accessions.split(',')[0]+"-other"+str(len(accessions))

response = batch.submit_job(jobName=tag+'-'+jobname, 
                            jobQueue=jobQueue, 
                            jobDefinition=jobDefinition, 
                            tags={"IndexThePlanet": tag},
                            propagateTags=True, #unfortunately it doesn't propagate to the EC2 instance :(
                            timeout={
                                'attemptDurationSeconds': int(threads)*3500 
                            },
                            containerOverrides={
                                "command": [ "python", "simple_batch_processor.py"],
                                "environment": [ 
                                    {"name": "Accessions", "value": accessions},
                                    {"name": "Region", "value": region},
                                    {"name": "Threads", "value": threads },
                                    {"name": "OutputBucket", "value": output_bucket },
                                    #{"name": "OnlyUnitigs", "value": "True" }, # CHANGEME
                                ]
                            })


print("Job ID is {}.".format(response['jobId']))
