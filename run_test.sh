#!/bin/bash
#bucket=logan-canada
bucket=$(if [[ -z $(aws sts get-caller-identity |grep serratus-rayan) ]]; then echo "logan-dec2023-testbucket"; else echo "logan-testing-march2024"; fi)
date=$(date +"%b%d-%Y")
arch=$(uname -m)
tag=cuttlefisha-$arch-$date-test

# Check for --heavier parameter
if [ "$1" == "--heavier" ]; then
    accession="SRR5057100"
    size=7600
else
	if [ "$1" == "--heaviest" ]; then
	    #accession="SRR10025398"
	    #size=58000

	    #human 1000G:
	    accession="ERR3239276"
	    size=118971
	else
	    accession="ERR485124"
	    size=1000
	fi
fi

aws s3 rm s3://$bucket/u/$accession --recursive
aws s3 rm s3://$bucket/c/$accession --recursive

cd batch
python3 simple_submit_job.py $accession $size $tag $bucket 13
