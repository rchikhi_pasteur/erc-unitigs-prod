#!/bin/bash
bucket=$(if [[ -z $(aws sts get-caller-identity |grep serratus-rayan) ]]; then echo "logan-dec2023-testbucket"; else echo "logan-testing-march2024"; fi)
jobqueue=$(if   [[ $(uname -m) == "aarch64" ]]; then echo "IndexThePlanetJobQueueGraviton";  else echo "IndexThePlanetJobQueue"; fi)

read -p "Will $(tput bold)erase$(tput sgr0) s3://$(tput bold)$bucket$(tput sgr0) folders u/ and c/ before continuing. (yes/no) " response

case "$response" in
    [yY][eE][sS]|[yY])
        # Continue with the rest of the script
        ;;
    *)
        echo "Exiting the script."
        exit 1
        ;;
esac

aws s3 rm s3://$bucket/u/ --recursive
aws s3 rm s3://$bucket/c/ --recursive
echo "060324-run5-subset60k"> set
bash process_array.sh $bucket $jobqueue
